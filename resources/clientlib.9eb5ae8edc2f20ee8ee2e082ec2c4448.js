$(function(){
  $('a[href^="#"]').on('click',function (e) {
      if($($(this).attr('href')).length != 0){
        scrollToElement($(this).attr('href'));
      }
    });
    function scrollToElement (selector) {
    $('html, body').animate({
      scrollTop: $(selector).offset().top-50
    }, 600);    
  };
})
