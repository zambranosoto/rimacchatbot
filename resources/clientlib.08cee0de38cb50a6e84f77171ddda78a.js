
        if (window.matchMedia("(max-width: 550px)").matches) {
            $('.razones__box__row').addClass('owl-theme owl-carousel')
            $('.razones__box__row').owlCarousel({
                loop: false,
                margin: 16,
                nav: false,
                items: 1,
                dots: false,
                autoWidth: false,
                slideBy: 1,
            })

            var owl = $('.razones__box__row');
            owl.owlCarousel();
            // Go to the next item
            $('.customNextBtn').click(function () {
                owl.trigger('next.owl.carousel');
            })
            // Go to the previous item
            $('.customPrevBtn').click(function () {
                owl.trigger('prev.owl.carousel', [300]);
            })
        }