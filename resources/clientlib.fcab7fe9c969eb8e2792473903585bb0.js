$(window).on('scroll',function() {
  if(! $(window).scrollTop()==0){
    $('.mi-cuenta-desplegable').slideUp('fast');
    $('.contenedor-micuenta .overlay').removeClass('active');
    $('.contenedor-micuenta .overlay').fadeOut('fast');

    $('.cmp-header .rimac-header.header-on-banner, .cmp-header .rimac-header-public.header-on-banner').removeClass('on');
  }else{
    $('.cmp-header .rimac-header.header-on-banner,.cmp-header .rimac-header-public.header-on-banner').addClass('on');
  }
});
window.addEventListener('DOMContentLoaded', function() {
  if(! $(window).scrollTop()==0){
    $('.cmp-header .rimac-header.header-on-banner, .cmp-header .rimac-header-public.header-on-banner').removeClass('on');
  }
});

$('.contenedor-micuenta .overlay').click(function(){
  $('.mi-cuenta-desplegable').slideUp('fast');
  $('.contenedor-micuenta .overlay').fadeOut('fast');
})

$('.btn-my-account').click(function(){
  $('.mi-cuenta-desplegable').slideDown('fast');
  $('.contenedor-micuenta .overlay').fadeIn('fast');

})



$('.mi-cuenta-desplegable ul li').mouseover(function(){
  $(this).css('opacity','1');
  $(this).siblings().css('opacity','0.5');

})

$('.mi-cuenta-desplegable ul li').mouseout(function(){
  $('.mi-cuenta-desplegable ul li').css('opacity','1');
})
$(document).ready(function () {
    function setActionTag(group,category,name,label){
        digitalData.push({
            "action": {
              "group": group,
              "category": category,
              "name": name,
              "label": label
            },
            "event": "trackAction"
          });  
      }
    
    //Links menu mobile y desktop
    $(".rimac-header-private .navbar-nav .no-dropdown>a").click(function(){
        setActionTag("Navegación", "Menu Principal","Navegar",$(this).text().trim());
    })
     //Links menu mobile y desktop
     $(".rimac-header-private .navbar-nav .dropdown>a").click(function(){
        setActionTag("Navegación", "Menu Principal","Navegar",$(this).text().trim());
    })

    //Button mi perfil
    $(".rimac-header-private .button-section-container .user-menu a").click(function(){
        setActionTag("Navegación", "Menu Principal","Navegar","Mi Perfil");
    })

    //Opciones mi perfil dropdown
    $(".rimac-header-private .user-menu .user-dropdown .level2Li a").click(function(){
        setActionTag("Navegación", "Menu Principal","Mi Perfil - Navegar",$(this).text().trim());
    })

    //Menu Principal (Desktop)
    $(".rimac-header-private .navbar-nav .dropdown-menu").find(".navigationbar-item-list li a").click(function(){
        setActionTag("Navegación", "Menu Principal",$(this).parent().parent().parent().siblings().text().trim() +" - Navegar",$(this).text().trim());
    }) 
    
})
