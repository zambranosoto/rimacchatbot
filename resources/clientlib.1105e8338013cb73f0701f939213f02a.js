var buttons = $('.stickyButtonMode');

buttons.each(function (i, e) {
  $(e).sticky({bottomSpacing: 0, topSpacing: window.innerHeight, zIndex: 1});
});

function scrollOrRedirect(link = "") {
  if (link.trim()[0] == "#") {
    scrollIt(link)
  } else if (link !== "") {
    location.href = link
  }
}

function scrollIt(selector) {
  var element = document.querySelector(selector)
  window.scrollTo({
    'behavior': 'smooth',
    'left': 0,
    'top': element.offsetTop
  });
}
