//_jcr_content/renditions/mobile.

(function(){
    var mobileBpKey = 'mobile',
        desktopBpKey = 'desktop',
        widesktopBpKey= 'widesktop',
        initialBp = null,
        backgroundType = 'background',
        imgType = 'img';

    function checkWidth() {
        return window.getComputedStyle(document.body, ':before').content.replace(/"/g, '');
    }

    function setImageURL($_this, type, imageUrl, imageUrlMobile, imageUrlwidesktop) {
        function setBgImage(url) {
            if(url === 'none') {
                $_this.css('background-image', '');
            }
            else {
                $_this.css('background-image', 'url(' + url + ')');
            }

        }

        function setImage(url) {
            $_this.attr('src', url);
        }
        var callback = null;
        if (type === backgroundType) {
            callback = setBgImage;
        }
        else if (type === imgType) {
            callback = setImage;
        }

        if (imageUrlMobile) {
            if(imageUrlMobile === 'none') {
                $_this.css('background-image', '');
            }
            else {
                var tester = new Image();
                tester.onload= function () {
                    callback(imageUrlMobile);
                };
                tester.onerror = function(e){
                    callback(imageUrl);
                    e.stopPropagation();
                };
                tester.src=imageUrlMobile;
            }
        }
        else if (imageUrlwidesktop) {
            if(imageUrlwidesktop === 'none') {
                $_this.css('background-image', '');
            }
            else {
                var tester = new Image();
                tester.onload= function () {
                    callback(imageUrlwidesktop);
                };
                tester.onerror = function(e){
                    callback(imageUrl);
                    e.stopPropagation();
                };
                tester.src=imageUrlwidesktop;
            }
        }
        else {
            callback(imageUrl);
        }


    }

    function setImage($_this, type) {
        var imageUrl = $_this.data('rendition-image'),
            imageUrlMobile = imageUrl + '/_jcr_content/renditions/mobile',
            imageUrlwidesktop = imageUrl + '/_jcr_content/renditions/widesktop',
            hideMobile = $_this.data('rendition-hide-mobile'),
            hideDesktop = $_this.data('rendition-hide-desktop');

        if (type === backgroundType) {

            if(checkWidth() === mobileBpKey) {
                setImageURL($_this, type, imageUrl, (hideMobile ? 'none' : imageUrlMobile) );
            }
            else if (checkWidth() === desktopBpKey) {
                setImageURL($_this, type, (hideDesktop ? 'none' : imageUrl), null);
            }
            else if (checkWidth() === widesktopBpKey) {
                setImageURL($_this, type, (hideDesktop ? 'none' : imageUrl), null, (hideDesktop ? 'none' : imageUrlwidesktop));
            }
        }
        else if (type === imgType) {
            if(checkWidth() === mobileBpKey) {
                setImageURL($_this, type, imageUrl, (hideMobile ? 'none' : imageUrlMobile));
            }
            else if (checkWidth() === desktopBpKey) {
                setImageURL($_this, type, imageUrl, null);
            }
            else if (checkWidth() === widesktopBpKey) {
                setImageURL($_this, type,imageUrl, null, imageUrlwidesktop);
            }
        }

    }

    function setBgImages(){
        $('*[data-rendition="true"]').each(function (e,i) {
            var $_this = $(this),
                renditionType = $_this.data('rendition-type');

            setImage($_this, renditionType);
        });
    }

    function initialize() {
        setBgImages();
        initialBp = checkWidth();
    }

    $(window).resize(function () {
        if (checkWidth() !== initialBp) {
            setBgImages();
            initialBp = checkWidth();
        }
    });

    if ($('.edit-mode').length > 0) {
        $('body').bind("DOMSubtreeModified", function() {
            setBgImages();
            initialBp = checkWidth();
        });
    }


    initialize();
}());
