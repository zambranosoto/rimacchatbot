$('.corona-list').click(function() {
    $(this).toggleClass('active')
    $(this).siblings().removeClass('active')

})


focus();
var listener = addEventListener('blur', function() {
    if (document.activeElement === document.getElementById('iframe')) {
        window.digitalData.push({
            action: {
                group: "Landing - Coronavirus",
                category: "Sección – Prevención",
                name: "Clic",
                label: "Ver Video"
            },
            event: "trackAction"
        });
    }
});


$('.corona-dropdown .title').click(function() {
    console.log($(this).parent().parent().data('parent'))
    if (!$(this).hasClass("view")) {
        $(this).addClass('view')
        window.digitalData.push({
            action: {
                group: "Landing - Coronavirus",
                category: "Sección - " + $(this).parent().parent().data('parent'),
                name: "Clic",
                label: $(this).text().trim()
            },
            event: "trackAction"
        });

    }

})