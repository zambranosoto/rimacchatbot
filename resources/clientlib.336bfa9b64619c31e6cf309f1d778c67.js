var url = $('#btn-home-poliza-error').data('url');


$('#btn-home-poliza-error').click(function(){
    localStorage.removeItem('rememberedUser');
    localStorage.removeItem('policies');
    localStorage.removeItem('policyDetail');
    localStorage.removeItem('token');
    localStorage.removeItem('preSignature');
    localStorage.removeItem('signature');

    window.location = url;
})

window.addEventListener("load", function(event) {
    if(document.getElementById('quit') !== null){
        document.getElementsByClassName('header')[0].style.display = 'none';
        document.getElementsByClassName('footer-container')[0].style.display = 'none';    
    }
});