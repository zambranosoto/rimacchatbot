$('.sintomas-corona__test a').click(function() {

    window.digitalData.push({
        action: {
            group: "Landing - Coronavirus",
            category: "Sección – Síntomas",
            name: "Clic",
            label: "Hacer Descarte"
        },
        event: "trackAction"
    });

})

$('.sintomas-corona__card .link').click(function() {

    window.digitalData.push({
        action: {
            group: "Landing - Coronavirus",
            category: "Sección – Síntomas",
            name: "Clic",
            label: "Cuidado en casa"
        },
        event: "trackAction"
    });

})