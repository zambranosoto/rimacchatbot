$('.close').click(function(e) {
    e.preventDefault()
    $("#modal").fadeOut()
    $(".modalCoronavirus").addClass("modalCoronavirus__index")
    digitalData.push({
        "action": {
            "group": "Home",
            "category": "Popup – Coronavirus",
            "name": "Click botón",
            "label": "Cerrar"
        },
        "event": "trackAction"
    });
})

$('.normal-text a').click(function() {
    window.digitalData.push({
        action: {
            group: "Home",
            category: "Popup - Coronavirus",
            name: "Click - " + $(this).parent().parent().parent().data('parent'),
            label: $(this).text().trim(),
        },
        event: "trackAction"
    });

});