let currentPage, lastPage, nextPage, prevPage;

let counterOfTodos;

let persisted = 0;
let categoryCounter = 0;
let booly = true;
let pageChanged = false;

let arrayOfResults = [];
let jsonOfResults = {
	category: '',
	results: []
};

let arrayOfVisitedResults = [];

let scrollingTop = 0;

$(document).ready(function () {

	var extraUrlText = '';
	if (window.location.href.indexOf('wcmmode=disabled') > -1) {
		extraUrlText = '?wcmmode=disabled';
	} else {
		extraUrlText = '';
	}


	$('.search-input').keyup(function () {
		var url = $(this).data('redirect-url');

		checkCharsAndChangeText(event, (url ? url + '.html' + extraUrlText : window.location.href));
	});
	$('.search-input-uses').keyup(function () {
		var url = $(this).data('redirect-url');
		checkCharsAndChangeTextUsesSearch(event, (url ? url + '.html' + extraUrlText : window.location.href))
	});

	if (localStorage.searchValue) {
		$('.search-input').val(localStorage.searchValue);
		$('.title-of-links2').html('Resultados para ' + (localStorage.searchValue).bold() + ' (' + (JSON.parse(localStorage.searchResults)).length + ')');
	}

	if (localStorage.searchResults) {
		makeDom();
	}

	if (localStorage.searchResultsUses) {
		makeDomUses();
	}

}); // end ready

function checkCharsAndChangeText(e, link) {
	localStorage.removeItem("searchResults");
	localStorage.removeItem("searchValue");
	let value = $(e.target).val();
	if (value.length >= 3) {
		$('.title-of-links').text('ESTÁS BUSCANDO');

		$.ajax({
			type: 'GET',
			url: '/bin/SearchContentServlet',
			data: 'phrase=' + $(e.target).val(),
			success: function (resp) {
				addingResultsToFastAccess(resp, value, 5, 'searchForm');

				if (e.keyCode == 13) {
					localStorage.setItem('searchValue', value);
					localStorage.setItem('searchResults', JSON.stringify(resp));
					if (link.split('content')[1] != (document.location.href.split('content')[1])) {
						document.location.href = link;
					} else {

						$('.tablinks').each(function () {
							$(this).html($(this).html().split(' ')[0]);
						})

						if (localStorage.searchValue) {
							$('.search-input').val(localStorage.searchValue);
							$('.title-of-links2').html('Resultados para ' + (localStorage.searchValue).bold() + ' (' + (JSON.parse(localStorage.searchResults)).length + ')');
						}

						if (localStorage.searchResults) {
							fadeOut();
							makeDom();
						}
					}
				}
			},
			error: function (err) {
				addingResultsToFastAccess(err.responseText);
			}
		});
	} else {
		$('.title-of-links').text('ACCESOS RÁPIDOS');
		resetFastAccess('block');
	}
}

function checkCharsAndChangeTextUsesSearch(e, link) {
	localStorage.removeItem('searchValueUses');
	localStorage.removeItem('searchResultsUses');
	let value = $(e.target).val();
	if (value.length >= 1) {
		$('#search-uses .closing-icon').removeClass('no-display');
		if ($(window).width() < 991) {
			if (window.location.href.indexOf('home.html') > -1) {
				scrollingTop = 1220;
			} else {

			}
			$('html, body').scrollTop(scrollingTop);
		}
		if (value.length >= 3) {

			$('#search-uses .list-of-links').removeClass('no-display');

			$.ajax({
				type: 'GET',
				url: '/bin/SearchContentServlet',
				data: 'phrase=' + $(e.target).val() + '&tag=question',
				success: function (resp) {
					addingResultsToFastAccess(resp, value, 3, 'search-uses');

					if (e.keyCode == 13) {
						localStorage.setItem('searchValueUses', value);
						localStorage.setItem('searchResultsUses', JSON.stringify(resp));
						if (link.split('content')[1] != (document.location.href.split('content')[1])) {
							document.location.href = link;
						} else {
							if (localStorage.searchResultsUses) {
								$('.search-input-uses').val(localStorage.searchValueUses);
								$('#search-uses .search-input-uses').val('');
								makeDomUses();
							}
						}
					} else {
						if (localStorage.searchValueUses) {
							localStorage.removeItem('searchValueUses');
							localStorage.removeItem('searchResultsUses');
						}
					}
				},
				error: function (err) {
					addingResultsToFastAccess(err.responseText);
				}
			});
		} else {
			$('#search-uses .list-of-links').addClass('no-display');
		}
	} else {
		$('#search-uses .closing-icon').addClass('no-display');
	}
}

function resetFastAccess(display) {
	let parent = $('.search-component .item-link-list>div>ul');
	$(parent).css("display", display);

	let pp = $('.search-results-direct-links');
	$(pp).each(function () {
		$(this).remove();
	});
}

function addingResultsToFastAccess(json, value, amount, id) {
	let grandparent = $('#' + id + ' .item-link-list>div');

	resetFastAccess('none');

	var elem = '<ul class="search-results-direct-links"><li class="bullet">' +
		'</li></ul>';
	grandparent.append(elem);
	if (json) {
		if (json.length < amount) {
			amount = json.length;
		}
	} else {
		amount = 0;
	}
	if (amount != 0) {
		for (var i = 0; i < amount; i++) {
			var e = $('.search-results-direct-links').children();
			var child =
				'<ul class="list-items ">' +
				'<li class="item row">' +
				'<span class="col-xs-1 icon-container">' +
				'<i>' +
				'<svg xmlns="http://www.w3.org/2000/svg" width="15.622" height="15.622" viewBox="0 0 15.622 15.622">' +
				'<defs>' +
				'<style>' +
				'.a {' +
				'fill: #a9b1d1;' +
				'}' +

				'.b {' +
				'fill: #fff;' +
				'}' +
				'</style>' +
				'</defs>' +
				'<g transform="translate(-319.199 -245.196)">' +
				'<path class="a" d="M15.622,7.814a7.811,7.811,0,1,0-7.811,7.811A7.811,7.811,0,0,0,15.622,7.814Z"' +
				'transform="translate(319.199 245.193)" />' +
				'<path class="b"' +
				'd="M138.216,84.222a2.14,2.14,0,0,1,.361-1.14,2.78,2.78,0,0,1,1.054-.956,3.316,3.316,0,0,1,1.616-.379,3.452,3.452,0,0,1,1.516.317,2.458,2.458,0,0,1,1.015.861,2.113,2.113,0,0,1,.358,1.184,1.83,1.83,0,0,1-.2.882,2.7,2.7,0,0,1-.485.654q-.281.275-1.009.927a4.106,4.106,0,0,0-.323.323,1.308,1.308,0,0,0-.181.255,1.241,1.241,0,0,0-.092.231q-.033.115-.1.406a.659.659,0,0,1-.7.616.72.72,0,0,1-.518-.2.791.791,0,0,1-.21-.6,2.1,2.1,0,0,1,.562-1.5,9.36,9.36,0,0,1,.687-.654q.379-.331.548-.5a1.7,1.7,0,0,0,.284-.376.909.909,0,0,0,.115-.45,1.046,1.046,0,0,0-.352-.8,1.286,1.286,0,0,0-.909-.326,1.257,1.257,0,0,0-.959.329,2.62,2.62,0,0,0-.521.968q-.2.669-.764.669a.75.75,0,0,1-.559-.234A.715.715,0,0,1,138.216,84.222ZM141.1,90.71a.932.932,0,0,1-.63-.234.82.82,0,0,1-.269-.654.843.843,0,0,1,.26-.628.88.88,0,0,1,.639-.255.869.869,0,0,1,.882.882.826.826,0,0,1-.266.651A.9.9,0,0,1,141.1,90.71Z"' +
				'transform="translate(186.335 167.023)" />' +
				'</g>' +
				'</svg>' +
				'</i></span>' +
				'<span class="col-xs-10 text p3">' +
				'<a class="link-item" href="' + json[i]["jcr:path"] + '.html">' + json[i]["jcr:title"] + '</a>' +
				'</span>' +
				'</li></ul>';
			e.append(child);
		};

		let el = $('.search-component .search-results-direct-links .link-item');
		$(el).each(function () {
			let str = $(this).text();
			let modStr = $(this).html();
			$(this).html($(this).html().replace(modStr, boldPartOfTheLink(value, str)));
		});
	} else {
		var e = $('.search-results-direct-links').children();
		var child =
			'<ul class="list-items ">' +
			'<li class="item row">' +
			'<span class="col-xs-11 text p3">' +
			'<a class="link-item">' + 'No hay resultados para tu búsqueda' + '</a>' +
			'</span>' +
			'</li></ul>';
		e.append(child);
	}
}

let boldPartOfTheLink = function (str, originalText) {
	let boldedText;
	let transformed;
	let transformedAndCut = originalText;
	let cutText = originalText;
	let arr = [];
	let unBolded = originalText.toLowerCase();
	let re = new RegExp(str.toLowerCase(), 'g');
	if (unBolded.indexOf(str.toLowerCase()) > -1) {
		let count = unBolded.match(re || []).length;
		for (var i = 0; i < count; i++) {

			let position = cutText.toLowerCase().indexOf(str.toLowerCase());
			let length = str.length;
			transformed = matchCase(str, cutText.substring(position, position + length));
			transformedAndCut = cutText.substring(0, position) + transformed + cutText.substring(position + length);
			if (transformed != null) {
				cutText = (transformedAndCut.split(transformed))[1];
				arr.push((transformedAndCut.split(transformed))[0]);
				arr.push(transformed);
			}
		}
		arr.push(cutText);
		boldedText = arr.join('');
	} else {
		boldedText = originalText;
	}
	return boldedText;
}

let matchCase = function (text, pattern) {
	let result = '';
	for (var i = 0; i < text.length; i++) {
		var c = text.charAt(i);
		var p = pattern.charCodeAt(i);

		if (p >= 65 && (p < 65 + 26 || (p>=192 && p <=220))) {
			result += c.toUpperCase();
		} else {
			result += c.toLowerCase();
		}
	}

	return result.bold();
}


//add a click function to the magnify glasss to show and extend the search field and hide the menu

$(".search-container .search-icon").click(function () {
	$("#rimac-search-input-field input").val('');
	$('#gral-results .closing-icon').addClass('hiding');
	$(this).fadeOut();
	$('.header-buttons .button-section-container ul li:first-child').fadeOut();
	$("nav.navbar .container-fluid ").fadeOut();

	$(".search-container .search-icon-red, #rimac-search-input-field, #rimac-search-input-field input, #rimac.search-input-field .item-link-list").fadeIn(1000);
	$("#rimac-search-input-field input").focus();

});

$("#rimac-search-input-field input").blur(function () {
	fadeOut();
});

$("#search-uses input").blur(function () {

});

function fadeOut() {
	$("#rimac-search-input-field").fadeOut(function () {
		$("#rimac-search-input-field").fadeOut().removeClass('show');
		$('.search-input').val('');
		let pp = $('.search-results-direct-links');
		$(pp).each(function () {
			$(this).remove();
		});
		let parent = $('.search-component .item-link-list>div>ul');
		$(parent).css("display", "block");
		$("nav.navbar .container-fluid, .search-container .search-icon ").fadeIn();
		$('.header-buttons .button-section-container ul li:first-child').fadeIn();
		$('#gral-results .closing-icon').removeClass('hiding');
	});
}

$(document).keyup(function (e) {
	if (e.keyCode === 27) {
		$("nav.navbar .container-fluid, .search-container .search-icon ").fadeIn();
		$("#rimac-search-input-field").fadeOut();
	}
});


//----------------


// Changing tabs

function openFilter(evt, tab) {
	var i, tabcontent, tablinks, tabactive;
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	tabactive = document.getElementsByClassName(tab)
	for (i = 0; i < tabactive.length; i++) {
		tabactive[i].style.display = "block";
		evt.currentTarget.className += " active";
	}

	$(arrayOfResults).each(function () {
		if (this.category.toLowerCase() == tab.toLowerCase()) {
			var windowsWidth = $(window).width();
			setPageNumber(windowsWidth, this.results);
		}
	});
}

//making DOM on USES
function makeDomUses() {
	$('.current-page').html('0');
	$('.last-page').html('0');
	$('.results-uses').empty();
	$('.results-uses').removeClass('no-display');
	$('.no-results').addClass('no-display');
	$('.pager-control').removeClass('no-display');
	let results = JSON.parse(localStorage.searchResultsUses);
	$('.search-input-uses').val(localStorage.searchValueUses);
	$('#search-uses .search-input-uses').val('');
	let gp = $('.results-uses');

	if (results.length > 0) {
		$('#uses-result').removeClass('borderless');

		if ($(window).width() > 991) {
			let nmbr = Math.floor(results.length / 15);
			if ((results.length / 15) > nmbr) {
				lastPage = nmbr + 1;
			} else {
				lastPage = nmbr;
			}
		} else {
			let nmbr = Math.floor(results.length / 10);
			if ((results.length / 10) > nmbr) {
				lastPage = nmbr + 1;
			} else {
				lastPage = nmbr;
			}
		}

		currentPage = 1;
		nextPage = currentPage + 1;
		prevPage = currentPage - 1;
		if (prevPage < 1) {
			prevPage = 1;
		}
		if (nextPage > lastPage) {
			nextPage = lastPage;
			$('.right-arrow').addClass('disabled');
		}



		letsMakeSomeResults(results);

	} else {
		$('.title-of-links2').html('');
		$('#uses-result').addClass('borderless');
		$('.results-uses').addClass('no-display');
		$('.no-results').removeClass('no-display');
		$('.pager-control').addClass('no-display');
		$('.left-arrow').addClass('disabled');
		$('.right-arrow').addClass('disabled');
	}
}

//making DOM on general results
function makeDom() {
	$('.results').removeClass('no-display');
	$('.no-results').addClass('no-display');
	$('.pager-control').removeClass('no-display');
	let results = JSON.parse(localStorage.searchResults);
	$('.search-input').val(localStorage.searchValue);
	let gp = $('.results');

	if (results.length > 0) {
		createArray(results);

	} else {
		$('.results').addClass('no-display');
		$('.no-results').removeClass('no-display');
		$('.pager-control').addClass('no-display');
		$('.left-arrow').addClass('disabled');
		$('.right-arrow').addClass('disabled');
		$('.current-page').html('00');
		$('.last-page').html('00');
	}
}

function cleanInput(str) {
	let input = $('#' + str + ' input');
	$(input).val('');
	$('#' + str + ' .list-of-links').addClass('no-display');
	$('#search-uses .closing-icon.fa').addClass('no-display');
}

function createArray(result) {
	let windowsWidth = $(window).width();
	arrayOfResults = [];
	jsonOfResults = {
		category: '',
		results: []
	};

	jsonOfResults.category = 'varios';
	arrayOfResults.push(jsonOfResults);

	let el = $('.tablinks');

	$(el).each(function () {
		jsonOfResults = {
			category: '',
			results: []
		};
		jsonOfResults.category = $(this).html();
		arrayOfResults.push(jsonOfResults);
	});

	for (var i = 0; i < result.length; i++) {
		if (result[i]["cq:tags"]) {
			//[INI]
			if(result[i]["cq:tags"].length == 0){
				//No tiene grupo
				createElementsInVarios(result[i]);
			}else{
				let tagNombreGrupo;
				if(result[i]["cq:tags"].length > 1){
					//Tiene varios tags
					tagNombreGrupo = result[i]["cq:tags"].filter(tag => {
						if(tag.indexOf('/') > -1){
							if(tag.split('/')[1].toLowerCase() != 'question'){
								return tag;
							}
						}
					});
				}else{
					//Tiene un solo tags
					if(result[i]["cq:tags"][0].indexOf('/') > -1){
						if(result[i]["cq:tags"][0].split('/')[1].toLowerCase() != 'question'){
							tagNombreGrupo = result[i]["cq:tags"];
						}
					}
				}

				if(tagNombreGrupo){
					//Pertenece a un grupo
					let mostroEnGrupo = false;
					$(arrayOfResults).each(function () {
						if (this.category.toLowerCase() == (tagNombreGrupo[0].split('/')[1]).toLowerCase()) {
							if (result[i]["jcr:description"]) {
								if ($(window).width() > 991) {
									content = (result[i]["jcr:description"]).substring(0, 150) + '...';
								} else {
									content = (result[i]["jcr:description"]).substring(0, 75) + '...';
								}

							} else {
								content = 'No hay contenido disponible';
							}
							mostroEnGrupo = true;
							addElementToArray(result[i], this.results, content);
						}
					});
					if(!mostroEnGrupo){
						createElementsInVarios(result[i]);
					}
				}else{
					//No tiene grupo
					createElementsInVarios(result[i]);
				}
			}
			//[FIN]
			/*for (var n = 0; n < result[i]["cq:tags"].length; n++) {

				if (result[i]["cq:tags"][n].indexOf('/') > -1) {

					if (((result[i]["cq:tags"][n].split('/'))[1]).toLowerCase() != 'question') {

						$(arrayOfResults).each(function () {
							if (this.category.toLowerCase() == ((result[i]["cq:tags"][n].split('/'))[1]).toLowerCase()) {
								if (result[i]["htmlContent"]) {
									if ($(window).width() > 991) {
										content = (result[i]["htmlContent"]).substring(0, 150) + '...';
									} else {
										content = (result[i]["htmlContent"]).substring(0, 75) + '...';
									}

								} else {
									content = 'No hay contenido disponible';
								}

								addElementToArray(result[i], this.results, content);
							}
						});
					} else {
						createElementsInVarios(result[i]);
					}
				} else {
					createElementsInVarios(result[i]);
				}
			}*/
		} else {
			createElementsInVarios(result[i]);
		}
	}

	for (var x in arrayOfResults) arrayOfResults[x].category == "varios" ? arrayOfResults.push(arrayOfResults.splice(x, 1)[0]) : 0;

	for (var i = 0; i < arrayOfResults.length; i++) {
		if (arrayOfResults[i].category.toLowerCase() != 'todos') {
			let el1 = '<h4 class="sp-font">' + arrayOfResults[i].category + '</h4>';

			for (var n = 0; n < arrayOfResults[i].results.length; n++) {

				$(arrayOfResults).each(function () {
					if (this.category.toLowerCase() == 'todos') {
						this.results.push(arrayOfResults[i].results[n]);
					}
				});
			}
		}

	}


	$(arrayOfResults).each(function () {
		if (this.category == 'TODOS') {
			counterOfTodos = 0;
			for (let i = 0; i < this.results.length; i++) {
				if (this.results[i].toString().indexOf('<h4 class="sp-font">') < 0) {
					counterOfTodos++;
				}
			}
			let active = $('button.active');
			if (active.length >= 1) {
				$(active).removeClass('active');
			}
			setPageNumber(windowsWidth, this.results);
		}
	});





}

function setPageNumber(windowsWidth, results) {
	let nmbr = 0;
	booly = true;
	pageChanged = false;
	if (windowsWidth > 991) {
		nmbr = Math.floor(results.length / 15);
		if ((results.length / 15) > nmbr) {
			lastPage = nmbr + 1;
		} else {
			lastPage = nmbr;
		}
	} else {
		nmbr = Math.floor(results.length / 10);
		if ((results.length / 10) > nmbr) {
			lastPage = nmbr + 1;
		} else {
			lastPage = nmbr;
		}
	}

	currentPage = 1;
	nextPage = currentPage + 1;
	prevPage = currentPage - 1;

	if (prevPage < 1) {
		prevPage = 1;
		$('.left-arrow').addClass('disabled');
	} else {
		$('.left-arrow').removeClass('disabled');
	}
	if (nextPage > lastPage) {
		nextPage = lastPage;
		$('.right-arrow').addClass('disabled');
	} else {
		$('.right-arrow').removeClass('disabled');
	}

	let gp = $('.results');
	let parent = $('.tabcontent.todos');

	$(parent).empty();

	$($(gp).children()).each(function () {
		if (!$(this).hasClass('todos') && !$(this).hasClass('tab')) {
			$(this).remove();
		}
	});

	arrayOfVisitedResults = [];
	persisted = 0;

	if (windowsWidth > 991) {
		createDomPerPage(15 * currentPage - 15, currentPage, windowsWidth, arrayOfResults);
	} else {
		createDomPerPage(10 * currentPage - 10, currentPage, windowsWidth, arrayOfResults);
	}
}

function createDomPerPage(initialNumber, multiplier, width, results) {

	let gp = $('.results');
	let parent = $('.tabcontent.todos');
	let el = $('.tablinks');

	$(el).each(function () {
		let stringToCompare = $(this).html().split(' ')[0];

		if (stringToCompare == 'TODOS') {
			$(results).each(function () {
				let counting = 0;
				if (this.category == stringToCompare) {
					let checkingNewArray = 0;
					for (let i = 0; i < arrayOfVisitedResults.length; i++) {
						if (arrayOfVisitedResults[i].toString().indexOf('<h4 class="sp-font">') > -1) {
							checkingNewArray++;
						}
					}
					if ((arrayOfVisitedResults.length - checkingNewArray) <= initialNumber) {
						if (width > 991) {
							if (this.results.length > initialNumber) {
								var parentPage = '<div class="page-with-results page-number-' + multiplier + '"></div>';
								if ($('.page-with-results.page-number-' + multiplier).length == 0) {
									parent.append(parentPage);

									$('.page-with-results').each(function () {
										$(this).css('display', 'none');
									});
									$('.page-with-results.page-number-' + multiplier).css('display', 'block');

									parent = $('.page-number-' + multiplier);

									if (this.results.length >= ((15 * multiplier))) {
										for (var i = (initialNumber); i < ((15 * multiplier)); i++) {

											for (let a = persisted; a < results.length; a++) {
												if (booly) {
													persisted++;
													if (results[a].category != 'TODOS' && results[a].results.length > 0) {
														categoryCounter = 0;
														let el1 = '<h4 class="sp-font">' + results[a].category + '</h4>';
														booly = false;
														parent.append(el1);
														if (counting > 0) {
															var countVal = 15;
															$('.page-with-results.page-number-' + multiplier + ' .sp-font').each(function () {
																if ($(this).html().split('(')[1] != null && $(this).html().split('(')[1] != 'undefined') {
																	countVal -= parseInt(($(this).html().split('(')[1]).split(')')[0]);
																}
															});
															counting = 0;
															let el2 = $('.page-with-results.page-number-' + multiplier + ' .sp-font:contains(' + results[persisted - 1].category + ')');
															addNumber(el2, countVal);
														}
													}
												}
											}
											if (pageChanged) {
												let el1 = '<h4 class="sp-font">' + results[persisted - 1].category + '</h4>';
												parent.append(el1);
												pageChanged = false;
											}
											parent.append(this.results[i]);
											categoryCounter++;
											counting++;
											if (counting == 15) {
												let el1 = $('.page-with-results.page-number-' + multiplier + ' .sp-font:contains(' + results[persisted - 1].category + ')');
												addNumber(el1, counting);
											}
											if (categoryCounter == results[persisted - 1].results.length) {
												booly = true;
												let el1 = $('.page-with-results.page-number-' + multiplier + ' .sp-font:contains(' + results[persisted - 1].category + ')');
												addNumber(el1, counting);
											}
										}
									} else {
										for (var i = (initialNumber); i < this.results.length; i++) {
											for (let a = persisted; a < results.length; a++) {
												if (booly) {
													persisted++;
													if (results[a].category != 'TODOS' && results[a].results.length > 0) {
														categoryCounter = 0;
														let el1 = '<h4 class="sp-font">' + results[a].category + '</h4>';
														booly = false;
														parent.append(el1);
														if (counting > 0) {
															var countVal = 15 - counting;
															counting = 0;
															let el2 = $('.page-with-results.page-number-' + multiplier + ' .sp-font:contains(' + results[persisted - 1].category + ')');
															addNumber(el2, countVal);
														}
													}
												}
											}
											if (pageChanged) {
												let el1 = '<h4 class="sp-font">' + results[persisted - 1].category + '</h4>';
												parent.append(el1);
												pageChanged = false;
											}
											parent.append(this.results[i]);
											categoryCounter++;
											counting++;
											if (counting == 15) {
												let el1 = $('.page-with-results.page-number-' + multiplier + ' .sp-font:contains(' + results[persisted - 1].category + ')');
												addNumber(el1, counting);
											}
											if (categoryCounter == results[persisted - 1].results.length) {
												booly = true;
												let el1 = $('.page-with-results.page-number-' + multiplier + ' .sp-font:contains(' + results[persisted - 1].category + ')');
												addNumber(el1, counting);
											}
										}
									}
								} else {
									$('.page-with-results').each(function () {
										$(this).css('display', 'none');
									});
									$('.page-with-results.page-number-' + multiplier).css('display', 'block');
								}
							}

						} else {
							if (this.results.length > initialNumber) {
								var parentPage = '<div class="page-with-results page-number-' + multiplier + '"></div>';
								if ($('.page-with-results.page-number-' + multiplier).length == 0) {
									parent.append(parentPage);

									$('.page-with-results').each(function () {
										$(this).css('display', 'none');
									});
									$('.page-with-results.page-number-' + multiplier).css('display', 'block');

									parent = $('.page-number-' + multiplier);

									if (this.results.length >= ((10 * multiplier))) {
										for (var i = (initialNumber); i < ((10 * multiplier)); i++) {

											for (let a = persisted; a < results.length; a++) {
												if (booly) {
													persisted++;
													if (results[a].category != 'TODOS' && results[a].results.length > 0) {
														categoryCounter = 0;
														let el1 = '<h4 class="sp-font">' + results[a].category + '</h4>';
														booly = false;
														parent.append(el1);
														if (counting > 0) {
															var countVal = 10;
															$('.page-with-results.page-number-' + multiplier + ' .sp-font').each(function () {
																if ($(this).html().split('(')[1] != null && $(this).html().split('(')[1] != 'undefined') {
																	countVal -= parseInt(($(this).html().split('(')[1]).split(')')[0]);
																}
															});
															counting = 0;
															let el2 = $('.page-with-results.page-number-' + multiplier + ' .sp-font:contains(' + results[persisted - 1].category + ')');
															addNumber(el2, countVal);
														}
													}
												}
											}
											if (pageChanged) {
												let el1 = '<h4 class="sp-font">' + results[persisted - 1].category + '</h4>';
												parent.append(el1);
												pageChanged = false;
											}
											parent.append(this.results[i]);
											categoryCounter++;
											counting++;
											if (counting == 10) {
												let el1 = $('.page-with-results.page-number-' + multiplier + ' .sp-font:contains(' + results[persisted - 1].category + ')');
												addNumber(el1, counting);
											}
											if (categoryCounter == results[persisted - 1].results.length) {
												booly = true;
												let el1 = $('.page-with-results.page-number-' + multiplier + ' .sp-font:contains(' + results[persisted - 1].category + ')');
												addNumber(el1, counting);
											}
										}
									} else {
										for (var i = (initialNumber); i < this.results.length; i++) {
											for (let a = persisted; a < results.length; a++) {
												if (booly) {
													persisted++;
													if (results[a].category != 'TODOS' && results[a].results.length > 0) {
														categoryCounter = 0;
														let el1 = '<h4 class="sp-font">' + results[a].category + '</h4>';
														booly = false;
														parent.append(el1);
														if (counting > 0) {
															var countVal = 10 - counting;
															counting = 0;
															let el2 = $('.page-with-results.page-number-' + multiplier + ' .sp-font:contains(' + results[persisted - 1].category + ')');
															addNumber(el2, countVal);
														}
													}
												}
											}
											if (pageChanged) {
												let el1 = '<h4 class="sp-font">' + results[persisted - 1].category + '</h4>';
												parent.append(el1);
												pageChanged = false;
											}
											parent.append(this.results[i]);
											categoryCounter++;
											counting++;
											if (counting == 10) {
												let el1 = $('.page-with-results.page-number-' + multiplier + ' .sp-font:contains(' + results[persisted - 1].category + ')');
												addNumber(el1, counting);
											}
											if (categoryCounter == results[persisted - 1].results.length) {
												booly = true;
												let el1 = $('.page-with-results.page-number-' + multiplier + ' .sp-font:contains(' + results[persisted - 1].category + ')');
												addNumber(el1, counting);
											}
										}
									}
								} else {
									$('.page-with-results').each(function () {
										$(this).css('display', 'none');
									});
									$('.page-with-results.page-number-' + multiplier).css('display', 'block');
								}
							}
						}
					}
				}
			});
		} else {
			let pparent = $('.tabcontent.' + stringToCompare);
			if (pparent.length > 0) {
				$(pparent).empty();
			} else {
				pparent = '<div class="tabcontent ' + stringToCompare + '" style="display:none;"></div>';
				gp.append(pparent);
				pparent = $('.' + stringToCompare);
			}

			$(results).each(function () {
				if (this.category == stringToCompare) {
					if (width > 991) {
						if (this.results.length > initialNumber) {
							if (this.results.length >= ((15 * multiplier))) {
								for (var i = (initialNumber - 1); i < ((15 * multiplier)); i++) {
									pparent.append(this.results[i]);
								}
							} else {
								for (var i = (initialNumber); i < this.results.length; i++) {
									pparent.append(this.results[i]);
								}
							}
						}

					} else {
						if (this.results.length > initialNumber) {
							if (this.results.length >= ((10 * multiplier - 1))) {
								for (var i = (initialNumber - 1); i < ((10 * multiplier)); i++) {
									pparent.append(this.results[i]);
								}
							} else {
								for (var i = (initialNumber); i < this.results.length; i++) {
									pparent.append(this.results[i]);
								}
							}
						}
					}

				}
			});
		}
	});

	$('.current-page').html('0' + currentPage);
	$('.current-page').addClass('activePage');
	$('.last-page').html('0' + lastPage);

	let active = $('button.active').html();
	if (!active) {
		$('.results .tab').children('button').addClass('active');
		$('.tabcontent.todos').css('display', 'block');
	} else {
		$('.tabcontent.' + active.split(' ')[0]).css('display', 'block');
	}

	$(arrayOfResults).each(function () {
		if (this.category != 'TODOS' && this.category != 'varios') {
			var btn = $('button:contains(' + this.category + ')');
			$(btn).html((($(btn).html()).split('('))[0] + ' (' + this.results.length + ')');
			if ($(btn).html().indexOf('(0)') > -1) {
				$(btn).prop('disabled', true);
			} else {
				$(btn).prop('disabled', false);
			}
		}
	});
}

function letsMakeSomeResults(arrayOfQuestions) {
	$('.title-of-links2').html(arrayOfQuestions.length + ' Resultados encontrados');
	if (arrayOfQuestions.length == 1) {
		$('.title-of-links2').html(arrayOfQuestions.length + ' Resultado encontrado');
	}
	let parentOfQuestions = $('.results-uses');
	if ($(window).width > 991) {
		if (arrayOfQuestions.length > currentPage * 15) {
			for (var i = currentPage * 15 - 15; i < currentPage * 15; i++) {
				pasteElement(arrayOfQuestions[i], parentOfQuestions);
			}
		} else {
			for (var i = currentPage * 15 - 15; i < arrayOfQuestions.length; i++) {
				pasteElement(arrayOfQuestions[i], parentOfQuestions);
			}
		}
	} else {
		if (arrayOfQuestions.length > currentPage * 10) {
			for (var i = currentPage * 10 - 10; i < currentPage * 10; i++) {
				pasteElement(arrayOfQuestions[i], parentOfQuestions);
			}
		} else {
			for (var i = currentPage * 10 - 10; i < arrayOfQuestions.length; i++) {
				pasteElement(arrayOfQuestions[i], parentOfQuestions);
			}
		}
	}

	$('.current-page').html('0' + currentPage);
	$('.current-page').addClass('activePage');
	$('.last-page').html('0' + lastPage);
}

function pasteElement(el, parnt) {
	let el2 = '<div class="no-padding result-card">' +
		'<a href="' + el["jcr:path"] + '.html">' +
		'<h5>' + el["jcr:title"] + '</h5>' +
		'</a>' +
		'</div>';

	parnt.append(el2);
}

function createElementsInVarios(el) {
	$(arrayOfResults).each(function () {
		if (this.category.toLowerCase() == 'varios') {
			if (el["jcr:description"]) {
				if ($(window).width() > 991) {
					content = (el["jcr:description"]).substring(0, 150) + '...';
				} else {
					content = (el["jcr:description"]).substring(0, 75) + '...';
				}
			} else {
				content = 'No hay contenido disponible';
			}
			addElementToArray(el, this.results, content);
		}
	});
}

function addElementToArray(el, arr, content) {
	let el2 = '<div class="no-padding result-card">' +
		'<a href="' + el["jcr:path"] + '.html">' +
		'<h5>' + el["jcr:title"] + '</h5>' +
		'<p>' + content + '</p>' +
		'</a>' +
		'</div>';

	arr.push(el2);
}

function changeToPrevious() {
	currentPage = prevPage;
	nextPage = currentPage + 1;
	prevPage = currentPage - 1;
	if (prevPage < 1) {
		prevPage = 1;
		$('.left-arrow').addClass('disabled');
	} else {
		$('.left-arrow').removeClass('disabled');
	}
	if (nextPage > lastPage) {
		nextPage = lastPage;
		$('.right-arrow').addClass('disabled');
	} else {
		$('.right-arrow').removeClass('disabled');
	}
	pageChanged = true;

	if ($(window).width() > 991) {
		if (prevPage == currentPage) {
			createDomPerPage(15 * currentPage - 15, currentPage, $(window).width(), arrayOfResults);
		} else {
			createDomPerPage(15 * currentPage - 15, currentPage, $(window).width(), arrayOfResults);
		}
	} else {
		if (prevPage == currentPage) {
			createDomPerPage(10 * currentPage - 10, currentPage, $(window).width(), arrayOfResults);
		} else {
			createDomPerPage(10 * currentPage - 10, currentPage, $(window).width(), arrayOfResults);
		}
	}

	$('html, body').scrollTop(0)
}

function changeToNext() {
	currentPage = nextPage;
	nextPage = currentPage + 1;
	prevPage = currentPage - 1;
	if (prevPage < 1) {
		prevPage = 1;
		$('.left-arrow').addClass('disabled');
	} else {
		$('.left-arrow').removeClass('disabled');
	}
	if (nextPage > lastPage) {
		nextPage = lastPage;
		$('.right-arrow').addClass('disabled');
	} else {
		$('.right-arrow').removeClass('disabled');
	}

	pageChanged = true;

	if ($(window).width() > 991) {
		if (nextPage == currentPage) {
			createDomPerPage(15 * currentPage - 15, currentPage, $(window).width(), arrayOfResults);
		} else {
			createDomPerPage(15 * currentPage - 15, currentPage, $(window).width(), arrayOfResults);
		}
	} else {
		if (nextPage == currentPage) {
			createDomPerPage(10 * currentPage - 10, currentPage, $(window).width(), arrayOfResults);
		} else {
			createDomPerPage(10 * currentPage - 10, currentPage, $(window).width(), arrayOfResults);
		}
	}

	$('html, body').scrollTop(0)
}

function changeToPreviousOnUses() {
	currentPage = prevPage;
	nextPage = currentPage + 1;
	prevPage = currentPage - 1;
	if (prevPage < 1) {
		prevPage = 1;
		$('.left-arrow').addClass('disabled');
	} else {
		$('.left-arrow').removeClass('disabled');
	}
	if (nextPage > lastPage) {
		nextPage = lastPage;
		$('.right-arrow').addClass('disabled');
	} else {
		$('.right-arrow').removeClass('disabled');
	}

	$('html, body').scrollTop(0)
}

function changeToNextOnUses() {
	currentPage = nextPage;
	nextPage = currentPage + 1;
	prevPage = currentPage - 1;
	if (prevPage < 1) {
		prevPage = 1;
		$('.left-arrow').addClass('disabled');
	} else {
		$('.left-arrow').removeClass('disabled');
	}
	if (nextPage > lastPage) {
		nextPage = lastPage;
		$('.right-arrow').addClass('disabled');
	} else {
		$('.right-arrow').removeClass('disabled');
	}

	$('html, body').scrollTop(0)
}

function addNumber(element, value) {
	var index = element.html();
	$(element).html($(element).html().split('(')[0] + ' (' + value + ')');

	for (var i = 0; i < arrayOfVisitedResults.length; i++) {
		if (arrayOfVisitedResults[i].indexOf('<h4 class="sp-font">' + index + '</h4>') > -1) {
			arrayOfVisitedResults.splice(i, 1, '<h4 class="sp-font">' + $(element).html() + '</h4>');
		}
	}
}