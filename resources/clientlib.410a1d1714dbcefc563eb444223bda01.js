const active = (elem) => {
    var a = document.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
        a[i].classList.remove('active')
    }
    elem.classList.add('active');
}
$(window).scroll(function() {
    var y = $(this).scrollTop();
    if (y > 280) {
        $('.solicita-navbar').fadeIn();
    } else {
        $('.solicita-navbar').fadeOut();
    }
});