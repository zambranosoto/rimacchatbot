$("#spinOn").click(()=>{
	let num = Math.floor(Math.random() * 100) + 1;
	let deg;
	console.log(num);
	if(num <= 10){
		deg = "385";
    }
	else if(num > 10 && num <=30){
		deg="335";
    }
	else if(num > 30 && num <=45){
		deg ="486";
    }
	else if(num > 45 && num <=55){
		deg ="432";
	}
	else if(num > 55 && num <=70){
		deg ="586";
	}
	else if(num > 70 && num <=85){
		deg ="532";
	}
	else if(num > 85 && num <=100){
		deg ="278";
	}
	console.log("deg", deg);
	const style = document.createElement('style');

	var keyFrames = '@-webkit-keyframes spin {100% {' +
        ' -webkit-transform: rotate('+deg+'deg); }}@keyframes spin {\
        100% {transform: rotate('+deg+'deg);   }}';
	style.innerText = keyFrames;
    document.head.appendChild(style);
})