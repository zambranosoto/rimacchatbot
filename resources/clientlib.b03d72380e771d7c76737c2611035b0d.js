$('.corona-proteccion-card__text a').click(function() {

    window.digitalData.push({
        action: {
            group: "Landing - Coronavirus",
            category: "Sección – Cobertura y servicios",
            name: "Clic",
            label: $(this).text().trim()
        },
        event: "trackAction"
    });
})

$('.corona-proteccion-test a').click(function() {

    window.digitalData.push({
        action: {
            group: "Landing - Coronavirus",
            category: "Sección – Cobertura y servicios",
            name: "Clic",
            label: "Ver Planes"
        },
        event: "trackAction"
    });
})