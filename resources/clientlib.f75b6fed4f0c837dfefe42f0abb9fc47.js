$(document).ready(function () {
	let el = $('.product-card');
	$(el).each(function () {
		let $this = this;
		let child = $(this).children('.color-border');
		if ($(child).hasClass('no-display')) {
			$($this).css('margin-top', '35px');
		}
	});

}); // end ready

