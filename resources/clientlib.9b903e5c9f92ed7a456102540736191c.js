$(document).ready(function(){
    const $target = $('.slide-nov');
    $target.css('width','100%');
    $('.slider-novedades').owlCarousel({
        items:1,
        dots:false,
        autoWidth:false,
        responsive:{
            330: {
                items:2,
                autoWidth:true,
                margin:16,
            },
            600:{
                items:3,
                autoWidth:false,
                mouseDrag:false,
                dots: false,
                margin:16
            }
        }
    })
})
