if (window.matchMedia("(max-width: 550px)").matches) {
    $('.solicita-chofer__detail__slides__column__slider__box').addClass('owl-theme owl-carousel')
    $('.solicita-chofer__detail__slides__column__slider__box').owlCarousel({
        loop: false,
        margin: 16,
        nav: false,
        items: 1,
        dots: false,
        autoWidth: false,
        slideBy: 1,
        autoplay: true,

    })
}
$('.solicita-chofer__detail__slides__images__box').addClass('owl-theme owl-carousel')
$('.solicita-chofer__detail__slides__images__box').owlCarousel({
    loop: false,
    margin: 20,
    nav: false,
    items: 1,
    dots: false,
    autoWidth: false,
    autoplay: true,
    autoplayTimeout: 5000,

})

$("#inicia").click(function(e) {
    e.preventDefault();
    $('.solicita-chofer__detail__slides__images__box').trigger('to.owl.carousel', [0, 500]);
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
});
$("#selecciona").click(function(e) {
    e.preventDefault();
    $('.solicita-chofer__detail__slides__images__box').trigger('to.owl.carousel', [1, 500]);
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
});
$("#registra").click(function(e) {
    e.preventDefault();
    $('.solicita-chofer__detail__slides__images__box').trigger('to.owl.carousel', [2, 500]);
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
});
$('.solicita-chofer__detail__slides__images__box').on('changed.owl.carousel', function(event) {
    console.log(event.item.index)
    if (event.item.index === 0) {
        $("#inicia").addClass('active');
        $("#selecciona").removeClass('active');
        $("#registra").removeClass('active');
        $('#number1').addClass('active');
        $('#number2').removeClass('active');
        $('#number3').removeClass('active');
    }
    if (event.item.index === 1) {
        $("#inicia").removeClass('active');
        $("#selecciona").addClass('active');
        $("#registra").removeClass('active');
        $('#number1').removeClass('active');
        $('#number2').addClass('active');
        $('#number3').removeClass('active');
    }
    if (event.item.index === 2) {
        $("#inicia").removeClass('active');
        $("#selecciona").removeClass('active');
        $("#registra").addClass('active');
        $('#number1').removeClass('active');
        $('#number2').removeClass('active');
        $('#number3').addClass('active');
    }
})