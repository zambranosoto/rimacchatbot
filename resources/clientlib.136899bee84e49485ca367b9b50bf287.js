if (window.matchMedia("(min-width: 920px)").matches) {
    $('.rimac-header-public .click-change-page').mouseout(function () {
        $('.rimac-header-public .click-change-page').parent().css('opacity', '1');
        $('.rimac-header-public .level1').removeClass('active');
    })
    $('.rimac-header-public .click-change-page').mouseover(function () {
        $(this).parent().css('opacity', '1');
        $('.rimac-header-public .level1').addClass('active');
        $(this).parent().siblings().css('opacity', '0.5');
    })
    $('.rimac-header-public .dropdown').mouseover(function () {
        $(this).addClass('open');
        $('.overlay-menu').addClass('active')
    })
    $('.rimac-header-public .dropdown').mouseout(function () {
        $(this).removeClass('open');
        $('.overlay-menu').removeClass('active')
    })
} else {
   $('.rimac-header-public .flecha').click(function(){
       $(this).siblings('.subitems').find('.dropdown-menu').addClass('openlevel');
       $('.rimac-header-public .areaback').addClass('active');
        if($(this).siblings('.subitems').find('.dropdown-menu').find('.retro').length){
        }
        else{
            var seguro = $(this).siblings('.btn-text-level1').text();
            $(this).siblings('.subitems').find('.dropdown-menu').prepend('<li class="retro" >'+seguro+'</li>');
        }
    });
    $('.rimac-header-public .btn-text-level1').click(function(){
        $(this).siblings('.subitems').find('.dropdown-menu').addClass('openlevel');
        $('.rimac-header-public .areaback').addClass('active');
         if($(this).siblings('.subitems').find('.dropdown-menu').find('.retro').length){
         }
         else{
             var seguro = $(this).text();
             $(this).siblings('.subitems').find('.dropdown-menu').prepend('<li class="retro" >'+seguro+'</li>');
         }
     });

  



     $('.rimac-header-public .areaback').click(function(){
        $('.rimac-header-public .level2').removeClass('openlevel');
        $('.rimac-header-public .areaback').removeClass('active');
    })

   $('.rimac-header-public .level2Li').click(function(){
    $(this).find('.level3').slideToggle('slow');
    $(this).siblings().find('.level3').slideUp('slow');
    $(this).toggleClass('active');
});

$('.rimac-header-public .level3 ul').addClass('desplegable');
$('.rimac-header-public .desplegable').parent().parent().addClass('desplegable');

}