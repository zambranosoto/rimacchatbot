/****************************************************/
/********remove after pr merge of last story*********/
function removeError(el) {
    $('.' + el).removeClass('form-error');
}
function checkNotEmpty(e) {
    if ($(e.target).val() == null || $(e.target).val() == '') {
        $(e.target).parent().addClass('form-error');
    } else {
        removeError($(e.target).parent());
    }
}
/****************************************************/

function selectInput() {
    var val = $('.form-dni .dni-kind').val();
    $('#toDestroy').remove();
    $('.dni-number.disabled').css('display', 'none');
    if (val == 'dni') {
        $('.dni-number.dni').css('display', 'block');
        $('.dni-number.extranjeria').css('display', 'none');
    } else if (val == 'extranjería') {
        $('.dni-number.extranjeria').css('display', 'block');
        $('.dni-number.dni').css('display', 'none');
    }
}

function setNewError(e) {
    console.log(e);
    $(e.target).attr('disabled', true);
    var p = $(e.target).parent();
    var el = '<span id="toDestroy" style="color: red; font-size: 10px; display: block !important">Debe seleccionar tipo de documento</span>';
    p.append(el);
}

$(document).ready(function () {
    $('form select, form input, form label, form textarea').click(function () {
        var el = $(this).parent().children('label');
        var focus = $(this).parent().children('input');
        $(el).addClass('focused');
        if (focus) {
            $(focus).focus();
        }
        $(this).on('blur', function () {
            if ($(this).val() == null || $(this).val() == '') {
                $(el).removeClass('focused');
            }
        });
    });
});

function countCharacters(e) {
    var num = 0;
    if ($('#charCount').text() != null && $('#charCount').text() != "") {
        console.log($('#charCount').text());
        num = parseInt($('#charCount').text());
    }
    if (num >= 100) {
        e.preventDefault();

    } else {
        num++;
        $('#charCount').text(num);
    }
}