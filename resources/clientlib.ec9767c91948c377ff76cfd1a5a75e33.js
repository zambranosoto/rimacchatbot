if (window.matchMedia("(max-width: 550px)").matches) {
    $('.tutoriales__list').addClass('owl-theme owl-carousel')
    $('.tutoriales__list').owlCarousel({
        loop: true,
        margin: 16,
        nav: false,
        items: 1,
        dots: false,
        autoWidth: true,
        slideBy: 1,
        autoplay: false,

    })
}
$('.tutoriales__list').addClass('owl-theme owl-carousel')
$('.tutoriales__list').owlCarousel({
    loop: false,
    margin: 32,
    nav: false,
    items: 3,
    dots: false,
    autoWidth: true,
    slideBy: 1,
    autoplay: false,

})
var owl = $('.owl-carousel');
owl.owlCarousel();
$('.customNextBtn').click(function() {
    owl.trigger('next.owl.carousel');
})
$('.customPrevBtn').click(function() {
    owl.trigger('prev.owl.carousel', [300]);
})