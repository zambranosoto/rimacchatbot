$(document).ready(function() {
  function setActionTag(group, category, name, label) {
    digitalData.push({
      action: {
        group,
        category,
        name,
        label
      },
      event: "trackAction"
    });
  }

  $(".contenedor-primero .side-text>a").click(function() {
    setActionTag(
      "Landing - Mi Cuenta Personas",
      "Coberturas",
      "click",
      "Ver ahora"
    );
  });

  $(".contenedor-segundo .side-text>a").click(function() {
    setActionTag(
      "Landing - Mi Cuenta Personas",
      "Clínicas y Talleres",
      "click",
      "Ver ahora"
    );
  });
  $(".contenedor-tercero .side-text>a").click(function() {
    setActionTag(
      "Landing - Mi Cuenta Personas",
      "Estados de pagos",
      "click",
      "Ver ahora"
    );
  });
});
