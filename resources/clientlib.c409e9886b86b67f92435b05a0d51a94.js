$(document).ready(function () {
    var userInfo = localStorage.getItem('rememberedUser');
    var target = $('.custom-title .custom-title-flex .logged-username');
    if(userInfo && $(target).data("useloggedusername")){
        user = JSON.parse(userInfo);
        target.text(user.nombreUsuario + "!");
    }
});
