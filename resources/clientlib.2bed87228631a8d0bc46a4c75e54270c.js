var acc = document.getElementsByClassName("class-100-widthtl");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", toggleMenuUntoggle);
    acc[i].addEventListener("touchstart", toggleMenuUntoggle);
}

function toggleMenuUntoggle(evt) {
    evt.preventDefault()
    if (window.innerWidth <= 991) {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    }
}
$('.informacion__files-box__files__column li a').click(function() {
    window.digitalData.push({
        action: {
            group: "Landing - Coronavirus",
            category: "Sección – Documentos extras",
            name: "Clic",
            label: $(this).text().trim(),
        },
        event: "trackAction"
    });

});