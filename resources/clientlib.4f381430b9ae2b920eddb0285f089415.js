
$(document).ready(function() {
 
    $('#btn-answer-no').click(function(){
        $('.modal-private-home-pop').fadeOut();
        localStorage.setItem('modalPrivateHome','activate');
        digitalData.push({
            "action":{
            "group":"Home",
            "category":"Popovers",
            "name":"Cliente Rimac",
            "label":"No"
            },
            "event":"trackAction"
           });
           digitalData.push({
            "profile":{
            "customer":"No",
            },
            "event":"trackProfile"
           });
    })
    
    $('#private-overlay').click(function(){
        $('.modal-private-home-pop').fadeOut();
        localStorage.setItem('modalPrivateHome','activate');
    })
    $('#btn-answer-yes').click(function(){
    
        $('.modal-private-home-pop__initial').addClass('active');
        $('.modal-private-home-pop__end').addClass('active fadein');
        $('.contenedor-principal').addClass('active');
    
        digitalData.push({
            "action":{
            "group":"Home",
            "category":"Popovers",
            "name":"Cliente Rimac",
            "label":"Si"
            },
            "event":"trackAction"
           });
           digitalData.push({
            "profile":{
            "customer":"Si",
            },
            "event":"trackProfile"
           });
    })
    $('.modal-private-home-pop__close').click(function(){
        $('.modal-private-home-pop').fadeOut();
        localStorage.setItem('modalPrivateHome','activate');
    })
    
    $('#btn-micuenta-modal').click(function(){
        localStorage.setItem('modalPrivateHome','activate');

        digitalData.push({
            "action":{
            "group":"Home",
            "category":"Popovers",
            "name":"Cliente Rimac",
            "label":"Ir a Mi Cuenta"
            },
            "event":"trackAction"
           });
    })
    
    if(!localStorage.getItem('modalPrivateHome')){
        $('.modal-private-home-pop').fadeIn();
    
    }
    
    
});