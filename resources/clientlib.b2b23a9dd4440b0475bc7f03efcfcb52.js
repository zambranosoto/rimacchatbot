$(function(){
    //accordion items
    let seemore= $(".see-more");
    let seeless= $(".see-less");
    let accordioncontent= $(".accordion-content");
    //initialize
    accordioncontent.hide();
    seemore.show();
    //events
    seemore.click(function(){
        seemore.hide(); 
        seeless.show();
        accordioncontent.slideDown("slow");
    });
    seeless.click(function(){
        seeless.hide();
        seemore.show(); 
        accordioncontent.slideUp();
    });

});
