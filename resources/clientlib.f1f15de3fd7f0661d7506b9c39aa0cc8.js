/*jshint strict: false, quotmark: false, nomen: false, unused: vars*/
/*global jQuery, Storage*/

(function (global) {
  var util = {};
  var pageLoaded = false;

  util.onload = function (fn) {
    if ("document" in global && document.readyState === "complete" || pageLoaded) {
      return fn();
    }
    util.on(global, "load", fn, false);
  };

  util.on = function (element, event, fn, capture) {
    if (element.attachEvent) {
      element.attachEvent("on" + event, fn);
    } else if (element.addEventListener) {
      element.addEventListener(event, fn, capture);
    }
  };

  /* our code */
  var host = "chat1-cls1-cgn-bct.i6.inconcertcc.com";
  var public_port = parseInt("80", 10);
  var public_ssl_port = parseInt("443", 10);
  var account = JSON.parse('{"website_id":"WEBCHAT_rimac@rimac_RimacChat","campaignId":"rimac","icr_display_name":"Agente Rimac","announce_entry":false,"vcc":"rimac","token":"1277462B88711D612ECC577DDE697D2D","app_name":"Rimac","urls":[],"campaignAtVcc":"rimac@rimac","snippet_parameters":[]}');
  var container_id = "chatmedico";
  var token = "1277462B88711D612ECC577DDE697D2D";
  var alternate_css = "";
  var tweety_app_name = "Rimac";
  var type = "popup";
  var googleConfiguration = null;
  var address;

  if (document.location.protocol === "https:") {
    address = "https://" + host + (public_ssl_port !== 443 ? ":" + public_ssl_port : "");
  } else {
    address = "http://" + host + (public_port !== 80 ? ":" + public_port : "");
  }

  // load json.js if needed (yes, you, IE7)
  if (!("JSON" in global)) {
    (function () {
      var node = document.createElement('script');
      node.type = 'text/javascript';
      node.async = true;
      node.src = address + '/scripts/json.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(node, s);
    })();
  }

  var got_jquery = function () {
    var $ = jQuery;

    function gatherParameters(account) {
      var parameters = [];
      for (var i = 0; i < account.snippet_parameters.length; i++) {
        var param = account.snippet_parameters[i];
        // we use short names so we don't exceed the GET url limit
        parameters[i] = {
          v: $("#" + param.controlId).val() || "",
          t: param.type,//"custom",
          a: param.alias,// "el alias"
          n: param.controlId
        };
      }
      return parameters;
    }

    //triggers
    var triggersConfiguration = [];
    var triggersInterval = null;

    function processTimeTrigger(trigger) {
      trigger.validPageSince = trigger.validPageSince || new Date();
      var diffSeconds = (new Date() - trigger.validPageSince) / 1000;
      return diffSeconds >= trigger.timeout;
    }

    function processTrigger(trigger, url) {
      if (!trigger.regex.test(url)) {
        trigger.validPageSince = null;
        return false;
      }
      switch (trigger.type) {
        case "time":
          return processTimeTrigger(trigger);
        default:
          return false;
      }
    }

    var processingTriggers = false;

    function processTriggers() {
      if (processingTriggers) {
        return;
      }
      processingTriggers = true;
      try {
        var url = window.location.href;
        $.each(triggersConfiguration, function (_, trigger) {
          if (processTrigger(trigger, url)) {
            initChat();
            return false;
          }
        });
      } catch (e) {
      }
      processingTriggers = false;
    }

    function startTriggers() {
      if (window.hasOwnProperty(token + "_triggers")) {
        triggersConfiguration = window[token + "_triggers"];
      }
      if (!triggersConfiguration.length) {
        return;
      }
      $.each(triggersConfiguration, function (_, trigger) {
        trigger.regex = new RegExp(trigger.regex, "i");
      });
      triggersInterval = window.setInterval(processTriggers, 1000);
    }

    function disableTriggers() {
      if (triggersInterval) {
        window.clearInterval(triggersInterval);
        triggersInterval = null;
      }
    }

    function initPopUpChat(url) {
      digitalData.push({
        "event":"trackDoctorChat"
      });
      global.open(url, "chat_window", "menubar=no,toolbar=no,location=no,resizable=yes,scrollbars=yes,status=no,height=500,width=400");
    }

    function closeIFrame() {
      $("#" + token).remove();
      $("#" + container_div_id).hide();
    }

    function notifyIFrame(iframeToken, source, event, data) {
      source.postMessage([iframeToken, event, data], "*");
    }

    //Se controla que la sesion no haya empezado hace mas de 3 horas
    //Es un filtro para evitar siempre validar sesiones caducadas contra el server
    function getSessionFunction(iframeToken) {
      var session = null;
      var sessionStored = (typeof (Storage) !== "undefined") ?
        localStorage.getItem(iframeToken + "_SESSION")
        : null;
      if (sessionStored) {
        var date_s = localStorage.getItem(iframeToken + "_SESSIONCREATED");
        if (date_s && parseInt(date_s)) {
          var date = new Date(parseInt(date_s));
          date.setHours(date.getHours() + 3);
          if (date > new Date()) {
            session = sessionStored;
          }
        }
      }

      if (session === null) {
        cleanEnvironment();
      }

      return session;
    }

    function getSession(source, iframeToken) {
      var session = getSessionFunction(iframeToken);
      notifyIFrame(iframeToken, source, "GET_SESSION_RESPONSE", session);
    }

    function getArgs(source, iframeToken) {
      var args = null;
      var session = getSessionFunction(iframeToken);
      if (session) {
        args = (typeof (Storage) !== "undefined") ?
          localStorage.getItem(iframeToken + "_ARGS")
          : null;
        if (args) {
          try {
            args = JSON.parse(args);
          } catch (e) {
            args = null;
          }
        }
        if (!args) {
          cleanEnvironment();
        }
      } else if (typeof (Storage) !== "undefined") {
        localStorage.setItem(iframeToken + "_ARGS", "");
      }
      notifyIFrame(iframeToken, source, "GET_ARGS_RESPONSE", args);
    }

    function setSession(session, iframeToken) {
      if (typeof (Storage) !== "undefined") {
        localStorage.setItem(iframeToken + "_SESSION", session);
        var now = new Date();
        localStorage.setItem(iframeToken + "_SESSIONCREATED", now.getTime());
      }
    }

    function setArgs(args, iframeToken) {
      if (typeof (Storage) !== "undefined") {
        localStorage.setItem(iframeToken + "_ARGS", JSON.stringify(args));
      }
    }

    function cleanEnvironment(iframeToken) {
      if (typeof (Storage) !== "undefined") {
        localStorage.setItem(iframeToken + "_SESSION", "");
        localStorage.setItem(iframeToken + "_SESSIONCREATED", "");
      }
    }

    function createIFrameEvent() {
      var iframeToken = token;

      window.addEventListener('message', function (event) {
        if (!$.isArray(event.data)) {
          return;
        }
        //en las transferencias cambiamos el token
        if (event.data[0] !== iframeToken) {
          if (event.data.length >= 2 && event.data[1] === "CHANGE_TOKEN") {
            iframeToken = event.data[0];
          }
          return;
        }

        var source = event.source;
        switch (event.data[1]) {
          case "CLOSE_IFRAME":
            return closeIFrame();
          case "GET_SESSION":
            return getSession(source, iframeToken);
          case "GET_ARGS":
            return getArgs(source, iframeToken);
          case "SET_SESSION":
            return setSession(event.data[2], iframeToken);
          case "SET_ARGS":
            return setArgs(event.data[2], iframeToken);
          case "CLEAN_ENVIRONMENT":
            return cleanEnvironment(iframeToken);
        }
      });
    }

    function initEmbeddedChat(url) {
      if ($("#" + token).length) {
        return;
      }
      var iframe = '<iframe id="' + token + '" width="100%" height="100%" style="border : none;" src="' + url + '" ></iframe>';
      var container = $("#" + container_div_id);
      if (!container.length) {
        $("body").append('<div id="' + container_div_id + '"></div>');
        container = $("#" + container_div_id);
      }
      $.each(container_div_css, function (prop, value) {
        if (value) {
          container.css(prop, value);
        }
      });
      container
        .html(iframe)
        .show();
    }

    function initChat() {
      disableTriggers();

      var url = address + "/inconcert/apps/webdesigner/"
        + encodeURIComponent(tweety_app_name)
        + "?token=" + encodeURIComponent(token)
        + "&type=" + (type || "popup");

      if (googleConfiguration) {
        url += "&google_key=" + encodeURIComponent(googleConfiguration.key)
          + "&google_bysections=" + encodeURIComponent(googleConfiguration.bySections);
      }

      if (alternate_css) {
        url += "&alt_css=" + encodeURIComponent(alternate_css);
      }

      var gatheredParameters = gatherParameters(account);
      if (gatheredParameters.length > 0) {
        url = url + "&params=" + encodeURIComponent(JSON.stringify(gatheredParameters));
      }
      switch (type) {
        case "embedded":
        case "fixed":
          return initEmbeddedChat(url);
        default:
          return initPopUpChat(url);
      }
    }

    $(document).ready(function () {
      var $container = $("." + container_id);
      $container.addClass("chatDisabled");

      // this piece of code was previously in Success.
      /* code */
      createIFrameEvent();
      startTriggers();
      var current_text = $container.text();
      $container
        .removeClass("chatDisabled")
        .addClass("chat")
        .unbind("click")

      $container.find("> a").click(
        function () {
          initChat();
          return false;
        });
      /* end code */

      $.ajax({
        url: address + "/is_campaign_active?token=" + token,
        type: "get",
        dataType: "jsonp",
        success: function (response) {
          if (response.status) {
          }
        },
        error: function (error) {
        }
      });
    });
  };

  // checks that jquery has loaded. Tests explictly for 'jQuery' since other libraries like MooTools use $ too.
  var checker = function () {
    return ("jQuery" in global);
  };

  if (checker()) {
    got_jquery();
  } else {
    // load jquery
    (function () {
      var node = document.createElement('script');
      node.type = 'text/javascript';
      node.async = true;
      node.src = address + '/scripts/jquery-1.4.2.min.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(node, s);
    })();
    var intId = setInterval(function () {
      if (checker()) {
        clearInterval(intId);
        got_jquery();
      }
    }, 100);
  }

})(this);
