function createCarousel() {
        /* $(".image-card.section").each(function () {
                $(this).removeClass("section");
        }); */

        //Initializing left carousel buttom on hide
        $(".left.carousel-control").hide();
        //Check for mobile
        if (window.innerWidth <= 992) {

                if (
                        $(".slideContainer .row-class .slick-carousel-inner").hasClass(
                                "cards-container"
                        )
                ) {

                        $(".slideContainer .row-class .slick-carousel-inner.cards-container").addClass(
                                "cards-container-removed"
                        );
                        $(
                                ".slideContainer .row-class .slick-carousel-inner.cards-container"
                        ).removeClass("cards-container");
                }

                try {
                        $(".slideContainer .row-class .slick-carousel-inner").slick({
                                dots: false,
                                infinite: false,
                                slidesToShow: 1.4,
                                slidesToScroll: 1,
                                centerMode: false,
                                mobileFirst: false,
                                focusOnSelect: true,
                                arrows: false,
                                responsive: [
                                        {
                                                breakpoint: 365,
                                                settings: {
                                                        slidesToShow: 1.4,
                                                        swipe:true
                                                }
                                        },
                                        {
                                                breakpoint: 335,
                                                settings: {
                                                        slidesToShow: 1.2,
                                                        swipe:true
                                                }
                                        }
                                ]
                        });
                } catch (error) {
                        //console.log("Already created");
                }
        }
        //Configuration for desktop
        if (window.innerWidth >= 992) {
                if (
                        $(".slideContainer .row-class .slick-carousel-inner").hasClass(
                                "cards-container-removed"
                        )
                ) {
                        $(
                                ".slideContainer .row-class .slick-carousel-inner.cards-container-removed"
                        ).addClass("cards-container");
                        $(
                                ".slideContainer .row-class .slick-carousel-inner.cards-container-removed"
                        ).removeClass("cards-container-removed");
                }
                try {
                        $(".slideContainer .row-class .slick-carousel-inner").slick("unslick");
                } catch (error) {
                        //console.log("Can't unslick unexistant element");

                }
        }
}

function beginC() {
        if (
                $(".slideContainer .row-class .slick-carousel-inner").hasClass(
                        "cards-container"
                ) &&
                window.innerWidth <= 992
        ) {
                $(".slideContainer .row-class .slick-carousel-inner.cards-container").addClass(
                        "cards-container-removed"
                );
                $(".slideContainer .row-class .slick-carousel-inner.cards-container").removeClass(
                        "cards-container"
                );
        }
        $(".slideClass .slick-carousel-inner").slick({
                dots: true,
                infinite: false,
                centerMode: false,
                slidesToShow: 3,
                slidesToScroll: 3,   
                draggable:false,
                touchMove:true,
                responsive: [
                        {
                                breakpoint: 1800,
                                settings: {
                                        swipe:false,
                                        swipeToSlide:false
                                }
                        },{
                                breakpoint: 991,
                                settings: {
                                        arrows: true,
                                        dots: true,
                                        centerMode: false,
                                        slidesToShow: 2,
                                        slidesToScroll: 2,
                                        swipe:true,
                                        swipeToSlide:true,
                                }
                        },
                        {
                                breakpoint: 767,
                                settings: {
                                        arrows: true,
                                        dots: true,
                                        centerMode: false,
                                        slidesToScroll: 1,
                                }
                        }
                ]
        });


        if (window.innerWidth > 992) {
                try {
                        $(".slideContainer .row-class .slick-carousel-inner").slick("unslick");
                } catch (error) {
                        //console.log("Can't unslick unexistant element");
                }
        } else {
                $(".slideContainer .row-class .slick-carousel-inner").slick({
                        dots: false,
                        infinite: false,
                        slidesToShow: 1.4,
                        slidesToScroll: 1,
                        centerMode: false,
                        mobileFirst: false,
                        focusOnSelect: false,
                        arrows: true,
                        responsive: [
                                {
                                        breakpoint: 1800,
                                        settings: {
                                                swipe:false,
                                                swipeToSlide:false
                                        }
                                },
                                {
                                        breakpoint: 991,
                                        settings: {
                                                swipe:true,
                                                swipeToSlide:true
                                        }
                                },
                                {
                                        breakpoint: 365,
                                        settings: {
                                                slidesToShow: 1.4,
                                                swipe:true
                                        }
                                },
                                {
                                        breakpoint: 335,
                                        settings: {
                                                slidesToShow: 1.2,
                                                swipe:true
                                        }
                                }
                                
                        ]
                });
        }
}
var $slickElement = $('.slideContainer .slick-carousel-inner');

$slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {

        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;

        this.parentElement.querySelector(".first-number").innerHTML = (i < 10)? "0" + i : i;

        if (slick.slideCount != null) {
                this.parentElement.querySelector(".second-number").innerHTML = (slick.slideCount < 10)? "0" + slick.slideCount : slick.slideCount;
        }
});
