function removeError(el) {
    $('.' + el).removeClass('form-error');
}

function checkEmail() {
    var email = $('.email').children('input').val();
    if (validateEmail(email)) {
        removeError('email');
    } else {
        $('.email').addClass('form-error');
    }
}

function checkNotEmpty(e) {
    if ($(e.target).val() == null || $(e.target).val() == '') {
        $(e.target).parent().addClass('form-error');
    } else {
        removeError($(e.target).parent());
    }
}

var validateEmail = function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

$(document).ready(function () {

    $('form select, form input, form label').click(function () {
        var el = $(this).parent().children('label');
        var focus = $(this).parent().children('input');
        $(el).addClass('focused');
        if (focus) {
            $(focus).focus();
        }
        $(this).on('blur', function () {
            if ($(this).val() == null || $(this).val() == '') {
                $(el).removeClass('focused');
            }
        });
    });
    $('.custom-checkbox').click(function () {
        $('.checkbox input').prop('checked', true);
        $('.custom-checkbox p').toggleClass('visibility-visible');
    });


    $(".phone-with-area-code .btn-group .dropdown-menu li a").click(function () {
        var selText = '<span class="areacode-number">' + $(this).text() + '</span>';
        var imgSource = $(this).find('img').attr('src');
        var img = '<img src="' + imgSource + '"/>';
        $(this).parents('.btn-group').find('.dropdown-toggle').html(img + ' ' + selText + ' <span class="caret"></span>');
    });
});

function callToNumber(number) {
    document.location.href = "tel:+" + number;
}

function showSuccessOrError() {
    $('.success').fadeIn('slow', function () {
        $('.modal').hide();
    })
    setTimeout(function () {
        $('.success').fadeOut('slow', function () {
            $('.custom-modal').modal('hide');
        });
    })
}