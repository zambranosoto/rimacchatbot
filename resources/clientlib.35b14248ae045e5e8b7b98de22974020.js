
jQuery.fn.ForceNumericOnly = function () {
    return this.each(function () {
        $(this).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            return (
                key == 8 ||
                key == 9 ||
                key == 46 ||
                (key >= 37 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};

$('.score-data').ForceNumericOnly();



$('#btn-modal-soccer').click(function (e) {
    let request = {
        uuid: localStorage.getItem('uuidUser'),
        homeTeam: $('#homeTeam').val(),
        awayTeam: $('#awayTeam').val()
    }
    $.ajax({
        type: 'post',
        url: 'https://g8d7ftrgs6.execute-api.us-east-2.amazonaws.com/prod/campaniaFutbol/do/aciertaTuPronostico',
        data: JSON.stringify(request),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            e.preventDefault();
            $('.img-modal').css('display', 'none');
            $('.modal-soccer-content_initial').css('display', 'none');
            $('.modal-soccer-content_thankyou').fadeIn();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            e.preventDefault();
            $('.img-modal').css('display', 'none');
            $('.modal-soccer-content_initial').css('display', 'none');
            $('.modal-soccer-content_thankyou').fadeIn();
        }
    });
});
$('#modalPrivateSoccer .overlay').click(function (e) {
    $('#modalPrivateSoccer').removeClass('active');
})

$('.close-modal-soccer').click(function (e) {
    e.preventDefault();
    $('#modalPrivateSoccer').removeClass('active');
});


$('.btn-modal-polla').click(function () {
    let request = {
        document: JSON.parse(localStorage.getItem('rememberedUser')).numeroDocumento
    }

    $.ajax({
        type: 'post',
        url: 'https://g8d7ftrgs6.execute-api.us-east-2.amazonaws.com/prod/campaniaFutbol/do/obtenerCampaniaFutbolera',
        data: JSON.stringify(request),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#subTitlePrivatePolla').text(data.subtitle);
            localStorage.setItem('uuidUser',data.uuid);
            for (var index = 0; index < data.team.length; index++) {
                var element = data.team[index];
                $('#'+element.type).val(element.score);
                $('#img'+element.type).attr('src',element.flag);
                $('#flag'+element.type).text(element.country);
                if(!data.max){
                    $('#'+element.type).attr('disabled','disabled');
                }
            }
            $('#modalPrivateSoccer').addClass('active');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#modalPrivateSoccer').removeClass('active');
        }
    });
})
