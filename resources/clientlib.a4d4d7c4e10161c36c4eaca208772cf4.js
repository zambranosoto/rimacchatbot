$(".dudas-si").click(function(e) {
    e.preventDefault()
    $(".dudas-content-question").addClass('hide-tablet-on-up')
    $(".dudas-content-yes").removeClass('hide-tablet-on-up')
    $(".dudas__buttons").addClass('hide-tablet-on-up')
})
$(".dudas-no").click(function(e) {
    e.preventDefault()
    $(".dudas-content-question").addClass('hide-tablet-on-up')
    $(".dudas-content-no").removeClass('hide-tablet-on-up')
    $(".dudas__buttons").addClass('hide-tablet-on-up')
})

if (window.matchMedia("(max-width: 550px)").matches) {
    $(".dudas-content-question").addClass('hide-tablet-on-up')
    $(".dudas-content-yes").removeClass('hide-tablet-on-up')
    $(".dudas-content-no").removeClass('hide-tablet-on-up')
    $(".dudas-content-yes").addClass('hide-mobile')
    $(".dudas-content-no").addClass('hide-mobile')

    $(".dudas-si").click(function(e) {
        e.preventDefault()
        $(".dudas-content-question").removeClass('hide-tablet-on-up')
        $(".dudas-content-question").addClass('hide-mobile')

        $(".dudas-content-yes").removeClass('hide-mobile')
        $(".dudas__buttons").removeClass('hide-tablet-on-up')
        $(".dudas__buttons").addClass('hide-mobile')
    })

    $(".dudas-no").click(function(e) {
        e.preventDefault()
        $(".dudas-content-question").removeClass('hide-tablet-on-up')
        $(".dudas-content-question").addClass('hide-mobile')

        $(".dudas-content-no").removeClass('hide-mobile')
        $(".dudas__buttons").removeClass('hide-tablet-on-up')
        $(".dudas__buttons").addClass('hide-mobile')
    })
}