$('.servicios-corona__btn').click(function() {
    var id = $(this).data('id')
    $('#' + id).toggleClass('active')
    $(this).toggleClass('active')
})

$('.servicios-corona__item').click(function() {
    var url = $(this).data('url')
    location.replace(url)


    digitalData.push({
        action: {
            group: "Landing - Coronavirus",
            category: "Sección – Cobertura y servicios",
            name: "Clic",
            label: $(this).find("p").text().trim()
        },
        event: "trackAction"
    });

})