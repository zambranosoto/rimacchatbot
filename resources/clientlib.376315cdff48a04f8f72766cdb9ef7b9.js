$(document).ready(function() {
  function setActionTag(group, category, name, label) {
    digitalData.push({
      action: {
        group,
        category,
        name,
        label
      },
      event: "trackAction"
    });
  }

  $("#button-ingresar-miCuenta").click(function() {
    setActionTag(
      "Landing - Mi Cuenta Personas",
      "Header",
      "click",
      "Ingresar ahora"
    );
  });

  $("#hero__link-scroll-micuenta").click(function() {
    setActionTag(
      "Landing - Mi Cuenta Personas",
      "Header",
      "click",
      "Ver cómo funciona"
    );
  });

  //make entire div clickeable for "como usar mi seguro" and "landing mi cuenta privada" on navbar
  $(
    ".rimac-header-public .header-navigation .navbar-link:nth-child(3) .navigationbar-item-list:nth-child(1) .click-change-page"
  ).click(function() {
    window.location = $(this)
      .find("a")
      .attr("href");
    return false;
  });

  $(
    ".rimac-header-public .header-navigation .navbar-link:nth-child(3) .navigationbar-item-list:nth-child(2) .click-change-page"
  ).click(function() {
    window.location = $(this)
      .find("a")
      .attr("href");
    return false;
  });

  //tag for button that redirect to coronavirus landing
  $(".button-video__container, .mobile__width-100").click(function() {
    setActionTag(
      "Landing - Me quedo en casa",
      "Cómo prevenir",
      "click",
      "Sobre coronavirus"
    );
  });

//position fixed for the section containning the image in the landing me quedo en casa
var windw = this;

$.fn.followTo = function ( elem ) {
    var $this = this,
        $window = $(windw),
        $bumper = $(elem),
        bumperPos = $bumper.offset().top,
        thisHeight = $this.outerHeight(),
        setPosition = function(){
            if ($window.scrollTop() > (bumperPos - thisHeight - 450 )) {
                $this.css({
                    position: 'absolute',
                    top:  bumperPos -  thisHeight - 750
                });
            }  else if ($window.scrollTop() <  250 ) {
                $this.css({
                  position: 'fixed',
                  top: 434 - $window.scrollTop()
                })
            } else {
                $this.css({
                    position: 'fixed',
                    top: 200
                });
            }
        };
    $window.resize(function()
    {
        bumperPos = pos.offset().top;
        thisHeight = $this.outerHeight();
        setPosition();
    });
    $window.scroll(setPosition);
    setPosition();
};
$('#image__section--landing').followTo('#video__section--landing '); 
});
