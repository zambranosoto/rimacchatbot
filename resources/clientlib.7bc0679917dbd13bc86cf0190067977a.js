(function(){
  var $header = $('header'),
    topSpacing = ($header.length > 0 ? $header[0].getBoundingClientRect().height + 50 : 0);

  $(".sticky-container").each(function (i, e) {
    var parent = $(e).closest('.aem-Grid')[0].offsetParent,
        parentEnd = parent.offsetTop + parent.offsetHeight,
        bottomSpacing = document.body.clientHeight - parentEnd + 250;
    $(e).sticky({topSpacing: topSpacing, bottomSpacing: bottomSpacing});

  });


  let itemIcons = $(" .border-red-sticky,  .icon-sticky-arrow");
  initialHide();

  $(".item-sticky-section").click(function () {
    hideIconItems();
    $(this).find(itemIcons).fadeIn();
    $(this).find(".item-text-sticky").addClass("red");
    $([document.documentElement, document.body]).animate({
      scrollTop: $($(this).find("a").attr("href")).offset().top -100
    }, 600);
    return false;
  });


  function hideIconItems(callback){
    itemIcons.hide();
    $(".item-text-sticky").removeClass("red");
    if(callback) callback();
  }
  function initialHide(){
    hideIconItems(function(){
      var firstItem = $( ".sticky-container .item-sticky-section" ).first();
      firstItem.find(itemIcons).fadeIn();
      firstItem.find(".item-text-sticky").addClass("red");
    })
  }

})()

