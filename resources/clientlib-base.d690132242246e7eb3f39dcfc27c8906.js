/*******************************************************************************
 * Copyright 2017 Adobe Systems Incorporated
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
if (window.Element && !Element.prototype.closest) {
    // eslint valid-jsdoc: "off"
    Element.prototype.closest =
        function(s) {
            "use strict";
            var matches = (this.document || this.ownerDocument).querySelectorAll(s);
            var el      = this;
            var i;
            do {
                i = matches.length;
                while (--i >= 0 && matches.item(i) !== el) {
                    // continue
                }
            } while ((i < 0) && (el = el.parentElement));
            return el;
        };
}

if (window.Element && !Element.prototype.matches) {
    Element.prototype.matches =
        Element.prototype.matchesSelector ||
        Element.prototype.mozMatchesSelector ||
        Element.prototype.msMatchesSelector ||
        Element.prototype.oMatchesSelector ||
        Element.prototype.webkitMatchesSelector ||
        function(s) {
            "use strict";
            var matches = (this.document || this.ownerDocument).querySelectorAll(s);
            var i       = matches.length;
            while (--i >= 0 && matches.item(i) !== this) {
                // continue
            }
            return i > -1;
        };
}

if (!Object.assign) {
    Object.assign = function(target, varArgs) { // .length of function is 2
        "use strict";
        if (target === null) {
            throw new TypeError("Cannot convert undefined or null to object");
        }

        var to = Object(target);

        for (var index = 1; index < arguments.length; index++) {
            var nextSource = arguments[index];

            if (nextSource !== null) {
                for (var nextKey in nextSource) {
                    if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
        }
        return to;
    };
}

(function(arr) {
    "use strict";
    arr.forEach(function(item) {
        if (item.hasOwnProperty("remove")) {
            return;
        }
        Object.defineProperty(item, "remove", {
            configurable: true,
            enumerable: true,
            writable: true,
            value: function remove() {
                this.parentNode.removeChild(this);
            }
        });
    });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);

/*******************************************************************************
 * Copyright 2016 Adobe Systems Incorporated
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
(function() {
    "use strict";

    var NS = "cmp";
    var IS = "image";

    var EMPTY_PIXEL = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    var LAZY_THRESHOLD = 0;
    var SRC_URI_TEMPLATE_WIDTH_VAR = "{.width}";

    var selectors = {
        self: "[data-" + NS + '-is="' + IS + '"]',
        image: '[data-cmp-hook-image="image"]',
        map: '[data-cmp-hook-image="map"]',
        area: '[data-cmp-hook-image="area"]'
    };

    var lazyLoader = {
        "cssClass": "cmp-image__image--is-loading",
        "style": {
            "height": 0,
            "padding-bottom": "" // will be replaced with % ratio
        }
    };

    var properties = {
        /**
         * An array of alternative image widths (in pixels).
         * Used to replace a {.width} variable in the src property with an optimal width if a URI template is provided.
         *
         * @memberof Image
         * @type {Number[]}
         * @default []
         */
        "widths": {
            "default": [],
            "transform": function(value) {
                var widths = [];
                value.split(",").forEach(function(item) {
                    item = parseFloat(item);
                    if (!isNaN(item)) {
                        widths.push(item);
                    }
                });
                return widths;
            }
        },
        /**
         * Indicates whether the image should be rendered lazily.
         *
         * @memberof Image
         * @type {Boolean}
         * @default false
         */
        "lazy": {
            "default": false,
            "transform": function(value) {
                return !(value === null || typeof value === "undefined");
            }
        },
        /**
         * The image source.
         *
         * Can be a simple image source, or a URI template representation that
         * can be variable expanded - useful for building an image configuration with an alternative width.
         * e.g. '/path/image.coreimg{.width}.jpeg/1506620954214.jpeg'
         *
         * @memberof Image
         * @type {String}
         */
        "src": {
        }
    };

    var devicePixelRatio = window.devicePixelRatio || 1;

    function readData(element) {
        var data = element.dataset;
        var options = [];
        var capitalized = IS;
        capitalized = capitalized.charAt(0).toUpperCase() + capitalized.slice(1);
        var reserved = ["is", "hook" + capitalized];

        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                var value = data[key];

                if (key.indexOf(NS) === 0) {
                    key = key.slice(NS.length);
                    key = key.charAt(0).toLowerCase() + key.substring(1);

                    if (reserved.indexOf(key) === -1) {
                        options[key] = value;
                    }
                }
            }
        }

        return options;
    }

    function Image(config) {
        var that = this;

        function init(config) {
            // prevents multiple initialization
            config.element.removeAttribute("data-" + NS + "-is");

            setupProperties(config.options);
            cacheElements(config.element);

            if (!that._elements.noscript) {
                return;
            }

            that._elements.container = that._elements.link ? that._elements.link : that._elements.self;

            unwrapNoScript();

            if (that._properties.lazy) {
                addLazyLoader();
            }

            if (that._elements.map) {
                that._elements.image.addEventListener("load", onLoad);
            }

            window.addEventListener("scroll", that.update);
            window.addEventListener("resize", onWindowResize);
            window.addEventListener("update", that.update);
            that._elements.image.addEventListener("cmp-image-redraw", that.update);
            that.update();
        }

        function loadImage() {
            var hasWidths = that._properties.widths && that._properties.widths.length > 0;
            var replacement = hasWidths ? "." + getOptimalWidth() : "";
            var url = that._properties.src.replace(SRC_URI_TEMPLATE_WIDTH_VAR, replacement);

            if (that._elements.image.getAttribute("src") !== url) {
                that._elements.image.setAttribute("src", url);
                if (!hasWidths) {
                    window.removeEventListener("scroll", that.update);
                }
            }

            if (that._lazyLoaderShowing) {
                that._elements.image.addEventListener("load", removeLazyLoader);
            }
        }

        function getOptimalWidth() {
            var container = that._elements.self;
            var containerWidth = container.clientWidth;
            while (containerWidth === 0 && container.parentNode) {
                container = container.parentNode;
                containerWidth = container.clientWidth;
            }
            var optimalWidth = containerWidth * devicePixelRatio;
            var len = that._properties.widths.length;
            var key = 0;

            while ((key < len - 1) && (that._properties.widths[key] < optimalWidth)) {
                key++;
            }

            return that._properties.widths[key].toString();
        }

        function addLazyLoader() {
            var width = that._elements.image.getAttribute("width");
            var height = that._elements.image.getAttribute("height");

            if (width && height) {
                var ratio = (height / width) * 100;
                var styles = lazyLoader.style;

                styles["padding-bottom"] = ratio + "%";

                for (var s in styles) {
                    if (styles.hasOwnProperty(s)) {
                        that._elements.image.style[s] = styles[s];
                    }
                }
            }
            that._elements.image.setAttribute("src", EMPTY_PIXEL);
            that._elements.image.classList.add(lazyLoader.cssClass);
            that._lazyLoaderShowing = true;
        }

        function unwrapNoScript() {
            var markup = decodeNoscript(that._elements.noscript.textContent.trim());
            var parser = new DOMParser();

            // temporary document avoids requesting the image before removing its src
            var temporaryDocument = parser.parseFromString(markup, "text/html");
            var imageElement = temporaryDocument.querySelector(selectors.image);
            imageElement.removeAttribute("src");
            that._elements.container.insertBefore(imageElement, that._elements.noscript);

            var mapElement = temporaryDocument.querySelector(selectors.map);
            if (mapElement) {
                that._elements.container.insertBefore(mapElement, that._elements.noscript);
            }

            that._elements.noscript.parentNode.removeChild(that._elements.noscript);
            if (that._elements.container.matches(selectors.image)) {
                that._elements.image = that._elements.container;
            } else {
                that._elements.image = that._elements.container.querySelector(selectors.image);
            }

            that._elements.map = that._elements.container.querySelector(selectors.map);
            that._elements.areas = that._elements.container.querySelectorAll(selectors.area);
        }

        function removeLazyLoader() {
            that._elements.image.classList.remove(lazyLoader.cssClass);
            for (var property in lazyLoader.style) {
                if (lazyLoader.style.hasOwnProperty(property)) {
                    that._elements.image.style[property] = "";
                }
            }
            that._elements.image.removeEventListener("load", removeLazyLoader);
            that._lazyLoaderShowing = false;
        }

        function isLazyVisible() {
            if (that._elements.container.offsetParent === null) {
                return false;
            }

            var wt = window.pageYOffset;
            var wb = wt + document.documentElement.clientHeight;
            var et = that._elements.container.getBoundingClientRect().top + wt;
            var eb = et + that._elements.container.clientHeight;

            return eb >= wt - LAZY_THRESHOLD && et <= wb + LAZY_THRESHOLD;
        }

        function resizeAreas() {
            if (that._elements.areas && that._elements.areas.length > 0) {
                for (var i = 0; i < that._elements.areas.length; i++) {
                    var width = that._elements.image.width;
                    var height = that._elements.image.height;

                    if (width && height) {
                        var relcoords = that._elements.areas[i].dataset.cmpRelcoords;
                        if (relcoords) {
                            var relativeCoordinates = relcoords.split(",");
                            var coordinates = new Array(relativeCoordinates.length);

                            for (var j = 0; j < coordinates.length; j++) {
                                if (j % 2 === 0) {
                                    coordinates[j] = parseInt(relativeCoordinates[j] * width);
                                } else {
                                    coordinates[j] = parseInt(relativeCoordinates[j] * height);
                                }
                            }

                            that._elements.areas[i].coords = coordinates;
                        }
                    }
                }
            }
        }

        function cacheElements(wrapper) {
            that._elements = {};
            that._elements.self = wrapper;
            var hooks = that._elements.self.querySelectorAll("[data-" + NS + "-hook-" + IS + "]");

            for (var i = 0; i < hooks.length; i++) {
                var hook = hooks[i];
                var capitalized = IS;
                capitalized = capitalized.charAt(0).toUpperCase() + capitalized.slice(1);
                var key = hook.dataset[NS + "Hook" + capitalized];
                that._elements[key] = hook;
            }
        }

        function setupProperties(options) {
            that._properties = {};

            for (var key in properties) {
                if (properties.hasOwnProperty(key)) {
                    var property = properties[key];
                    if (options && options[key] != null) {
                        if (property && typeof property.transform === "function") {
                            that._properties[key] = property.transform(options[key]);
                        } else {
                            that._properties[key] = options[key];
                        }
                    } else {
                        that._properties[key] = properties[key]["default"];
                    }
                }
            }
        }

        function onWindowResize() {
            that.update();
            resizeAreas();
        }

        function onLoad() {
            resizeAreas();
        }

        that.update = function() {
            if (that._properties.lazy) {
                if (isLazyVisible()) {
                    loadImage();
                }
            } else {
                loadImage();
            }
        };

        if (config && config.element) {
            init(config);
        }
    }

    function onDocumentReady() {
        var elements = document.querySelectorAll(selectors.self);
        for (var i = 0; i < elements.length; i++) {
            new Image({ element: elements[i], options: readData(elements[i]) });
        }

        var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
        var body             = document.querySelector("body");
        var observer         = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                // needed for IE
                var nodesArray = [].slice.call(mutation.addedNodes);
                if (nodesArray.length > 0) {
                    nodesArray.forEach(function(addedNode) {
                        if (addedNode.querySelectorAll) {
                            var elementsArray = [].slice.call(addedNode.querySelectorAll(selectors.self));
                            elementsArray.forEach(function(element) {
                                new Image({ element: element, options: readData(element) });
                            });
                        }
                    });
                }
            });
        });

        observer.observe(body, {
            subtree: true,
            childList: true,
            characterData: true
        });
    }

    if (document.readyState !== "loading") {
        onDocumentReady();
    } else {
        document.addEventListener("DOMContentLoaded", onDocumentReady());
    }

    /*
        on drag & drop of the component into a parsys, noscript's content will be escaped multiple times by the editor which creates
        the DOM for editing; the HTML parser cannot be used here due to the multiple escaping
     */
    function decodeNoscript(text) {
        text = text.replace(/&(amp;)*lt;/g, "<");
        text = text.replace(/&(amp;)*gt;/g, ">");
        return text;
    }

})();

/*******************************************************************************
 * Copyright 2017 Adobe Systems Incorporated
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
(function() {
    "use strict";

    var NS = "cmp";
    var IS = "search";

    var DELAY = 300; // time before fetching new results when the user is typing a search string
    var LOADING_DISPLAY_DELAY = 300; // minimum time during which the loading indicator is displayed
    var PARAM_RESULTS_OFFSET = "resultsOffset";

    var keyCodes = {
        TAB: 9,
        ENTER: 13,
        ESCAPE: 27,
        ARROW_UP: 38,
        ARROW_DOWN: 40
    };

    var selectors = {
        self: "[data-" + NS + '-is="' + IS + '"]',
        item: {
            self: "[data-" + NS + "-hook-" + IS + '="item"]',
            title: "[data-" + NS + "-hook-" + IS + '="itemTitle"]',
            focused: "." + NS + "-search__item--is-focused"
        }
    };

    var properties = {
        /**
         * The minimum required length of the search term before results are fetched.
         *
         * @memberof Search
         * @type {Number}
         * @default 3
         */
        minLength: {
            "default": 3,
            transform: function(value) {
                value = parseFloat(value);
                return isNaN(value) ? null : value;
            }
        },
        /**
         * The maximal number of results fetched by a search request.
         *
         * @memberof Search
         * @type {Number}
         * @default 10
         */
        resultsSize: {
            "default": 10,
            transform: function(value) {
                value = parseFloat(value);
                return isNaN(value) ? null : value;
            }
        }
    };

    var idCount = 0;

    function readData(element) {
        var data = element.dataset;
        var options = [];
        var capitalized = IS;
        capitalized = capitalized.charAt(0).toUpperCase() + capitalized.slice(1);
        var reserved = ["is", "hook" + capitalized];

        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                var value = data[key];

                if (key.indexOf(NS) === 0) {
                    key = key.slice(NS.length);
                    key = key.charAt(0).toLowerCase() + key.substring(1);

                    if (reserved.indexOf(key) === -1) {
                        options[key] = value;
                    }
                }
            }
        }

        return options;
    }

    function toggleShow(element, show) {
        if (element) {
            if (show !== false) {
                element.style.display = "block";
                element.setAttribute("aria-hidden", false);
            } else {
                element.style.display = "none";
                element.setAttribute("aria-hidden", true);
            }
        }
    }

    function serialize(form) {
        var query = [];
        if (form && form.elements) {
            for (var i = 0; i < form.elements.length; i++) {
                var node = form.elements[i];
                if (!node.disabled && node.name) {
                    var param = [node.name, encodeURIComponent(node.value)];
                    query.push(param.join("="));
                }
            }
        }
        return query.join("&");
    }

    function mark(node, regex) {
        if (!node || !regex) {
            return;
        }

        // text nodes
        if (node.nodeType === 3) {
            var nodeValue = node.nodeValue;
            var match = regex.exec(nodeValue);

            if (nodeValue && match) {
                var element = document.createElement("mark");
                element.className = NS + "-search__item-mark";
                element.appendChild(document.createTextNode(match[0]));

                var after = node.splitText(match.index);
                after.nodeValue = after.nodeValue.substring(match[0].length);
                node.parentNode.insertBefore(element, after);
            }
        } else if (node.hasChildNodes()) {
            for (var i = 0; i < node.childNodes.length; i++) {
                // recurse
                mark(node.childNodes[i], regex);
            }
        }
    }

    function Search(config) {
        if (config.element) {
            // prevents multiple initialization
            config.element.removeAttribute("data-" + NS + "-is");
        }

        this._cacheElements(config.element);
        this._setupProperties(config.options);

        this._action = this._elements.form.getAttribute("action");
        this._resultsOffset = 0;
        this._hasMoreResults = true;

        this._elements.input.addEventListener("input", this._onInput.bind(this));
        this._elements.input.addEventListener("focus", this._onInput.bind(this));
        this._elements.input.addEventListener("keydown", this._onKeydown.bind(this));
        this._elements.clear.addEventListener("click", this._onClearClick.bind(this));
        document.addEventListener("click", this._onDocumentClick.bind(this));
        this._elements.results.addEventListener("scroll", this._onScroll.bind(this));

        this._makeAccessible();
    }

    Search.prototype._displayResults = function() {
        if (this._elements.input.value.length === 0) {
            toggleShow(this._elements.clear, false);
            this._cancelResults();
        } else if (this._elements.input.value.length < this._properties.minLength) {
            toggleShow(this._elements.clear, true);
        } else {
            this._updateResults();
            toggleShow(this._elements.clear, true);
        }
    };

    Search.prototype._onScroll = function(event) {
        // fetch new results when the results to be scrolled down are less than the visible results
        if (this._elements.results.scrollTop + 2 * this._elements.results.clientHeight >= this._elements.results.scrollHeight) {
            this._resultsOffset += this._properties.resultsSize;
            this._displayResults();
        }
    };

    Search.prototype._onInput = function(event) {
        var self = this;
        self._cancelResults();
        // start searching when the search term reaches the minimum length
        this._timeout = setTimeout(function() {
            self._displayResults();
        }, DELAY);
    };

    Search.prototype._onKeydown = function(event) {
        var self = this;

        switch (event.keyCode) {
            case keyCodes.TAB:
                if (self._resultsOpen()) {
                    event.preventDefault();
                }
                break;
            case keyCodes.ENTER:
                event.preventDefault();
                if (self._resultsOpen()) {
                    var focused = self._elements.results.querySelector(selectors.item.focused);
                    if (focused) {
                        focused.click();
                    }
                }
                break;
            case keyCodes.ESCAPE:
                self._cancelResults();
                break;
            case keyCodes.ARROW_UP:
                if (self._resultsOpen()) {
                    event.preventDefault();
                    self._stepResultFocus(true);
                }
                break;
            case keyCodes.ARROW_DOWN:
                if (self._resultsOpen()) {
                    event.preventDefault();
                    self._stepResultFocus();
                } else {
                    // test the input and if necessary fetch and display the results
                    self._onInput();
                }
                break;
            default:
                return;
        }
    };

    Search.prototype._onClearClick = function(event) {
        event.preventDefault();
        this._elements.input.value = "";
        toggleShow(this._elements.clear, false);
        toggleShow(this._elements.results, false);
    };

    Search.prototype._onDocumentClick = function(event) {
        var inputContainsTarget =  this._elements.input.contains(event.target);
        var resultsContainTarget = this._elements.results.contains(event.target);

        if (!(inputContainsTarget || resultsContainTarget)) {
            toggleShow(this._elements.results, false);
        }
    };

    Search.prototype._resultsOpen = function() {
        return this._elements.results.style.display !== "none";
    };

    Search.prototype._makeAccessible = function() {
        var id = NS + "-search-results-" + idCount;
        this._elements.input.setAttribute("aria-owns", id);
        this._elements.results.id = id;
        idCount++;
    };

    Search.prototype._generateItems = function(data, results) {
        var self = this;

        data.forEach(function(item) {
            var el = document.createElement("span");
            el.innerHTML = self._elements.itemTemplate.innerHTML;
            el.querySelectorAll(selectors.item.title)[0].appendChild(document.createTextNode(item.title));
            el.querySelectorAll(selectors.item.self)[0].setAttribute("href", item.url);
            results.innerHTML += el.innerHTML;
        });
    };

    Search.prototype._markResults = function() {
        var nodeList = this._elements.results.querySelectorAll(selectors.item.self);
        var escapedTerm = this._elements.input.value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
        var regex = new RegExp("(" + escapedTerm + ")", "gi");

        for (var i = this._resultsOffset - 1; i < nodeList.length; ++i) {
            var result = nodeList[i];
            mark(result, regex);
        }
    };

    Search.prototype._stepResultFocus = function(reverse) {
        var results = this._elements.results.querySelectorAll(selectors.item.self);
        var focused = this._elements.results.querySelector(selectors.item.focused);
        var newFocused;
        var index = Array.prototype.indexOf.call(results, focused);
        var focusedCssClass = NS + "-search__item--is-focused";

        if (results.length > 0) {

            if (!reverse) {
                // highlight the next result
                if (index < 0) {
                    results[0].classList.add(focusedCssClass);
                } else if (index + 1 < results.length) {
                    results[index].classList.remove(focusedCssClass);
                    results[index + 1].classList.add(focusedCssClass);
                }

                // if the last visible result is partially hidden, scroll up until it's completely visible
                newFocused = this._elements.results.querySelector(selectors.item.focused);
                if (newFocused) {
                    var bottomHiddenHeight = newFocused.offsetTop + newFocused.offsetHeight - this._elements.results.scrollTop - this._elements.results.clientHeight;
                    if (bottomHiddenHeight > 0) {
                        this._elements.results.scrollTop += bottomHiddenHeight;
                    } else {
                        this._onScroll();
                    }
                }

            } else {
                // highlight the previous result
                if (index >= 1) {
                    results[index].classList.remove(focusedCssClass);
                    results[index - 1].classList.add(focusedCssClass);
                }

                // if the first visible result is partially hidden, scroll down until it's completely visible
                newFocused = this._elements.results.querySelector(selectors.item.focused);
                if (newFocused) {
                    var topHiddenHeight = this._elements.results.scrollTop - newFocused.offsetTop;
                    if (topHiddenHeight > 0) {
                        this._elements.results.scrollTop -= topHiddenHeight;
                    }
                }
            }
        }
    };

    Search.prototype._updateResults = function() {
        var self = this;
        if (self._hasMoreResults) {
            var request = new XMLHttpRequest();
            var url = self._action + "?" + serialize(self._elements.form) + "&" + PARAM_RESULTS_OFFSET + "=" + self._resultsOffset;

            request.open("GET", url, true);
            request.onload = function() {
                // when the results are loaded: hide the loading indicator and display the search icon after a minimum period
                setTimeout(function() {
                    toggleShow(self._elements.loadingIndicator, false);
                    toggleShow(self._elements.icon, true);
                }, LOADING_DISPLAY_DELAY);
                if (request.status >= 200 && request.status < 400) {
                    // success status
                    var data = JSON.parse(request.responseText);
                    if (data.length > 0) {
                        self._generateItems(data, self._elements.results);
                        self._markResults();
                        toggleShow(self._elements.results, true);
                    } else {
                        self._hasMoreResults = false;
                    }
                    // the total number of results is not a multiple of the fetched results:
                    // -> we reached the end of the query
                    if (self._elements.results.querySelectorAll(selectors.item.self).length % self._properties.resultsSize > 0) {
                        self._hasMoreResults = false;
                    }
                } else {
                    // error status
                }
            };
            // when the results are loading: display the loading indicator and hide the search icon
            toggleShow(self._elements.loadingIndicator, true);
            toggleShow(self._elements.icon, false);
            request.send();
        }
    };

    Search.prototype._cancelResults = function() {
        clearTimeout(this._timeout);
        this._elements.results.scrollTop = 0;
        this._resultsOffset = 0;
        this._hasMoreResults = true;
        this._elements.results.innerHTML = "";
    };

    Search.prototype._cacheElements = function(wrapper) {
        this._elements = {};
        this._elements.self = wrapper;
        var hooks = this._elements.self.querySelectorAll("[data-" + NS + "-hook-" + IS + "]");

        for (var i = 0; i < hooks.length; i++) {
            var hook = hooks[i];
            var capitalized = IS;
            capitalized = capitalized.charAt(0).toUpperCase() + capitalized.slice(1);
            var key = hook.dataset[NS + "Hook" + capitalized];
            this._elements[key] = hook;
        }
    };

    Search.prototype._setupProperties = function(options) {
        this._properties = {};

        for (var key in properties) {
            if (properties.hasOwnProperty(key)) {
                var property = properties[key];
                if (options && options[key] != null) {
                    if (property && typeof property.transform === "function") {
                        this._properties[key] = property.transform(options[key]);
                    } else {
                        this._properties[key] = options[key];
                    }
                } else {
                    this._properties[key] = properties[key]["default"];
                }
            }
        }
    };

    function onDocumentReady() {
        var elements = document.querySelectorAll(selectors.self);
        for (var i = 0; i < elements.length; i++) {
            new Search({ element: elements[i], options: readData(elements[i]) });
        }

        var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
        var body = document.querySelector("body");
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                // needed for IE
                var nodesArray = [].slice.call(mutation.addedNodes);
                if (nodesArray.length > 0) {
                    nodesArray.forEach(function(addedNode) {
                        if (addedNode.querySelectorAll) {
                            var elementsArray = [].slice.call(addedNode.querySelectorAll(selectors.self));
                            elementsArray.forEach(function(element) {
                                new Search({ element: element, options: readData(element) });
                            });
                        }
                    });
                }
            });
        });

        observer.observe(body, {
            subtree: true,
            childList: true,
            characterData: true
        });
    }

    if (document.readyState !== "loading") {
        onDocumentReady();
    } else {
        document.addEventListener("DOMContentLoaded", onDocumentReady);
    }

})();

/*******************************************************************************
 * Copyright 2016 Adobe Systems Incorporated
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
(function() {
    "use strict";

    var NS = "cmp";
    var IS = "formText";
    var IS_DASH = "form-text";

    var selectors = {
        self: "[data-" + NS + '-is="' + IS + '"]'
    };

    var properties = {
        /**
         * A validation message to display if there is a type mismatch between the user input and expected input.
         *
         * @type {String}
         */
        constraintMessage: {
        },
        /**
         * A validation message to display if no input is supplied, but input is expected for the field.
         *
         * @type {String}
         */
        requiredMessage: {
        }
    };

    function readData(element) {
        var data = element.dataset;
        var options = [];
        var capitalized = IS;
        capitalized = capitalized.charAt(0).toUpperCase() + capitalized.slice(1);
        var reserved = ["is", "hook" + capitalized];

        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                var value = data[key];

                if (key.indexOf(NS) === 0) {
                    key = key.slice(NS.length);
                    key = key.charAt(0).toLowerCase() + key.substring(1);

                    if (reserved.indexOf(key) === -1) {
                        options[key] = value;
                    }
                }
            }
        }

        return options;
    }

    function FormText(config) {
        if (config.element) {
            // prevents multiple initialization
            config.element.removeAttribute("data-" + NS + "-is");
        }

        this._cacheElements(config.element);
        this._setupProperties(config.options);

        this._elements.input.addEventListener("invalid", this._onInvalid.bind(this));
        this._elements.input.addEventListener("input", this._onInput.bind(this));
    }

    FormText.prototype._onInvalid = function(event) {
        event.target.setCustomValidity("");
        if (event.target.validity.typeMismatch) {
            if (this._properties.constraintMessage) {
                event.target.setCustomValidity(this._properties.constraintMessage);
            }
        } else if (event.target.validity.valueMissing) {
            if (this._properties.requiredMessage) {
                event.target.setCustomValidity(this._properties.requiredMessage);
            }
        }
    };

    FormText.prototype._onInput = function(event) {
        event.target.setCustomValidity("");
    };

    FormText.prototype._cacheElements = function(wrapper) {
        this._elements = {};
        this._elements.self = wrapper;
        var hooks = this._elements.self.querySelectorAll("[data-" + NS + "-hook-" + IS_DASH + "]");
        for (var i = 0; i < hooks.length; i++) {
            var hook = hooks[i];
            var capitalized = IS;
            capitalized = capitalized.charAt(0).toUpperCase() + capitalized.slice(1);
            var key = hook.dataset[NS + "Hook" + capitalized];
            this._elements[key] = hook;
        }
    };

    FormText.prototype._setupProperties = function(options) {
        this._properties = {};

        for (var key in properties) {
            if (properties.hasOwnProperty(key)) {
                var property = properties[key];
                if (options && options[key] != null) {
                    if (property && typeof property.transform === "function") {
                        this._properties[key] = property.transform(options[key]);
                    } else {
                        this._properties[key] = options[key];
                    }
                } else {
                    this._properties[key] = properties[key]["default"];
                }
            }
        }
    };

    function onDocumentReady() {
        var elements = document.querySelectorAll(selectors.self);
        for (var i = 0; i < elements.length; i++) {
            new FormText({ element: elements[i], options: readData(elements[i]) });
        }

        var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
        var body = document.querySelector("body");
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                // needed for IE
                var nodesArray = [].slice.call(mutation.addedNodes);
                if (nodesArray.length > 0) {
                    nodesArray.forEach(function(addedNode) {
                        if (addedNode.querySelectorAll) {
                            var elementsArray = [].slice.call(addedNode.querySelectorAll(selectors.self));
                            elementsArray.forEach(function(element) {
                                new FormText({ element: element, options: readData(element) });
                            });
                        }
                    });
                }
            });
        });

        observer.observe(body, {
            subtree: true,
            childList: true,
            characterData: true
        });
    }

    if (document.readyState !== "loading") {
        onDocumentReady();
    } else {
        document.addEventListener("DOMContentLoaded", onDocumentReady());
    }

})();

// execute these functions when the page opens
function checkWidth() {
    return window.getComputedStyle(document.body, ':before').content.replace(/"/g, '');
}

$(document).ready(function () {

    addEditingMode();

    $('.user').click(function (event) {
        $('.square.inv').removeClass('no-display');
        $('.user .dropdown-menu.level2').addClass('clicked');
        event.stopPropagation();
    });

    $('html').click(function () {
        $('.square.inv').addClass('no-display');
        $('.user .dropdown-menu.level2').removeClass('clicked');
    });

    $('.not-icon').addClass('notification');
    addMenuOnMb();

    $(window).resize(function () {
        addMenuOnMb();
        if ($('.enterprise').length > 0) {
            changeClassLocation();
        }
    });

    if ($(window).width() < 991) {
        if ($('.enterprise').length > 0) {
            changeClassLocation();
        } else {

            $('.click-change-page').click(function () {
                var textOfTheLi = $(this).children('a').text();
                changePage(textOfTheLi);
            });
        }
    } else {
        $('.header-buttons .btn-phone').removeAttr("href");
    }

    $('.nav-icon1').click(function () {
        $(this).toggleClass('open');
    });

    //if windows.width < 1205, takes it as mobile and sets the first menu open
    if ($(window).width() < 1206 && $('.rimac-header-private').length <= 0) {
            $('.nav.navbar-nav li.dropdown:first-child ul.dropdown-menu').addClass('opened');
            $('.dropdown-menu.level2:not(.user-dropdown)').each(function () {
                var title = $(this).parent().children('a').html();
                var el = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ' + title.split(" ").join('') + ' " style="display:none;">' +

                    '</div>';

                $($(this).children('li')).each(function () {
                    var title2 = $(this).children('a').children('h4').html();
                    var el2 = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 third-lev-mobile">' +
                        '<a href="#" class="third-lev-mobile">' + title2 + '</a>' +
                        '</div>';

                    $(el).append(el2);
                });
                $('.navbar-collapse').append(el);

                var parent = $(this).parent().children('a');
                var arrow = '<span class="caret-right"></span>';
                $(parent).append(arrow);
            });

    } else if ($('.rimac-header-private').length > 0) {
        $('.nav.navbar-nav li.dropdown:first-child ul.dropdown-menu').addClass('opened');
        $('.dropdown-menu.level1:not(.user-dropdown)').each(function (i,e) {
            var title = $(this).parent().children('a').text(),
                elemId = "navigation-private-lv2-" + i,
                $el = $('<div id="' + elemId + '" class="col-xs-12 third-lev-container ' + title.split(" ").join('') + ' " style="display:none;">' +

                '</div>');
            var el1 = '<h4 class="third-lev-mobile main" onclick="gimme3rdLvlBack(\'' + elemId + '\')">' + title + '</h4>';
            $el.append(el1)

            $(this).find('.navigationbar-item-list > li').each(function () {
                var title2 = $(this).children('a').text();
                var url2mob = $(this).children('a').attr('href');
                var onclick = $(this).children('a').attr('onclick');
                
                if(url2mob){
                    var el2 = '<div class="third-lev-mobile">' +
                        '<a href="' + url2mob + '"' + 'class="third-lev-mobile">' + title2 + '</a>' + '</div>';
                        $el.append(el2);
                }else{
                    var el2 = '<div class="third-lev-mobile">' +
                        '<a onclick="' + onclick + '"' + 'class="third-lev-mobile">' + title2 + '</a>' + '</div>';
                        $el.append(el2);
                }
            });

            $('.navbar-collapse').append($el);

            var parent = $(this).parent().children('a');
            var arrow = '<i class="link-arrow-icon"><svg width="7" height="13" viewBox="0 0 7 13" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
                '    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.211956 1.31464C-0.0640881 1.02117 -0.0703039 0.559103 0.192158 0.25757C0.334337 0.0944671 0.53767 -2.35023e-08 0.752995 -3.29145e-08C0.957289 -4.18445e-08 1.15172 0.0847921 1.29345 0.235146L6.78764 5.96022C6.92876 6.11029 7 6.30665 7 6.50018C7 6.69375 6.92916 6.88963 6.78802 7.03966L1.29415 12.7644C0.988953 13.0883 0.481977 13.0755 0.192287 12.7429C-0.070334 12.4413 -0.0641135 11.9791 0.211991 11.6856L5.19895 6.50018L0.211956 1.31464Z"></path>\n' +
                '</svg>\n' +
                '</i>';
            $(parent).append(arrow);
            $(this).parent().click(function() {
                if (checkWidth() === 'mobile') changePage(elemId);
            });
        });
        $('.level2-mi-cuenta-mob').append('<li class="level2Li hide-element-desktop">' +
            '                        <a href="#"  onclick="logout()" role="button">Cerrar sesión</a>' +
            '                    </li>');
        var title = $('.navbar-nav .user-menu .user .btn-profile').text(),
            elemId = "navigation-user-lv2-",
            $el = $('<div id="' + elemId + '" class="col-xs-12 third-lev-container ' + title.trim().split(" ").join('') + ' " style="display:none;"></div>');
        var el1 = '<h4 class="third-lev-mobile main user-name-in-menu" onclick="gimme3rdLvlBack(\'' + elemId + '\')">' + title + '</h4>';
        $el.append(el1);
        $('.navbar-nav .dropdown-menu.level2.user-dropdown .level2Li').each(function (i,e) {
            var title2 = $(this).children('a').text();
            var el2 = '<div class="third-lev-mobile">' +
                '<a href="#" class="third-lev-mobile">' + title2 + '</a>' +
                '</div>';

            $el.append(el2);
        });

        $('.navbar-nav .user-menu .user .btn-profile').click(function() {
            if (checkWidth() === 'mobile') changePage(elemId);
        });
        $('.navbar-collapse').append($el);
        $('.header-buttons .user-menu .dropdown-menu').append('<li class=" level2Li hide-element-mobile">' +
            '            <a href="#" onclick="logout()">' +
            '            Cerrar sesión' +
            '        </a>\n' +
            '        </li>');
    }

    //Menu Principal (Mobile)
    $(".rimac-header-private").find(".third-lev-container div a").click(function(){
        setActionTag("Navegación", "Menu Principal",$(this).parent().parent().children(":first").text().trim() +" - Navegar",$(this).text().trim());
    })

    if ($('.hero').length <= 0) {
        $('header').addClass('background-rimac');
    }

    $(window).scroll(function () {
        addEditingMode();
        var width = $(window).width();
        if ($('.hero').length >= 1) {
            if (width >= 1206) {
                if ($('html').scrollTop() >= 77) {
                    $('header').addClass('background-rimac');
                } else {
                    $('header').removeClass('background-rimac');
                }
            } else {
                if ($('html').scrollTop() >= 48) {
                    $('header').addClass('background-rimac');
                } else {
                    $('header').removeClass('background-rimac');
                }
            }
        } else {
            $('header').addClass('background-rimac');
        }

    });

    $(function () {
        $('.image-card-container.vertical .wrapper-text').matchHeight();
    });

    $(function () {
        $('.product-card-content').matchHeight({byRow: 0});
    });

    setUserName();
});

function setUserName() {
    var $userNameContainer = $('.user-name-in-menu');
    if ($userNameContainer.length > 0) {
        var user = JSON.parse(localStorage.getItem('rememberedUser'));
        $userNameContainer.text(user ? ( (user.nombreUsuario || '') ) : '');
    }

}

function logout() {
    if (!localStorage.getItem('loggedInUser')) {
        localStorage.removeItem('rememberedUser');
    }

    if(localStorage.getItem('datosLogin')){

        var redirect = JSON.parse(localStorage.getItem('datosLogin'));

        var urlLogout = redirect.urlLogin.indexOf('.html');

        var redirectPage, urlLogin;

        if (urlLogout == -1) {
             urlLogin = `${redirect.urlLogin}.html`
             redirectPage = `${redirect.urlLogin}.logout.html`;
        } else {
            urlLogin = redirect.urlLogin;
            redirectPage = redirect.urlLogin.replace('.html','.logout.html');
        }

        // perform redirect to login page.
        if(redirectPage && urlLogin){
            window.location.href = redirectPage+"?resource="+urlLogin;
        }else{
            console.log("Redirect page not applied. Missing convention call and the URLs.");
        }
    }else{
        console.log("Missing 'datosLogin' object, redirect not applied.");
    }

    setUserName();
}

function addMenuOnMb() {
    var userMenu = $('.user-menu'),
        navbarUserMenu = $('.navbar-nav .user-menu');

    if (userMenu.length > 0 && navbarUserMenu.length === 0) {
        var newUserMenu = userMenu.clone();
        newUserMenu.find('.btn-profile svg').replaceWith('<svg width="7" height="13" viewBox="0 0 7 13" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.211956 1.31464C-0.0640881 1.02117 -0.0703039 0.559103 0.192158 0.25757C0.334337 0.0944671 0.53767 -2.35023e-08 0.752995 -3.29145e-08C0.957289 -4.18445e-08 1.15172 0.0847921 1.29345 0.235146L6.78764 5.96022C6.92876 6.11029 7 6.30665 7 6.50018C7 6.69375 6.92916 6.88963 6.78802 7.03966L1.29415 12.7644C0.988953 13.0883 0.481977 13.0755 0.192287 12.7429C-0.070334 12.4413 -0.0641135 11.9791 0.211991 11.6856L5.19895 6.50018L0.211956 1.31464Z"></path>\n' +
            '</svg>');
        var newSvg = newUserMenu.find('.btn-profile svg').clone();
        newUserMenu.find('.btn-profile svg').remove();
        newUserMenu.find('.btn-profile').append(newSvg);
        $('ul.nav.navbar-nav').prepend(newUserMenu);
    }
}





//add a click function on menu link buttons
$('nav.navbar.navbar-default div ul li.dropdown a').click(function () {
    removeOpacity();

    var el = $('.nav.navbar-nav li.dropdown.open .dropdown-menu .dropdown-menu.level2');

    addClass('no-display', el);
});



//first, make sure no sublinks have the opacity to 50%.
function removeOpacity() {
    $('.nav.navbar-nav li .dropdown-menu li a').each(function () {
        if (!$(this).parent().parent().hasClass('level2')) {
            if ($(this).hasClass('opacity-50')) {
                $(this).removeClass('opacity-50');
            }
        }
    });
}

//then, on hover of a sublink, pass by each sublink and set the font opacity to .5, 
//then set the one with the hover to 1
function watchForHoverOnSubLink() {
    if ($(window).width() >= 1206 && $('.rimac-header-private').length <= 0) {
        var el = $('.rimac-header-public .nav.navbar-nav li.dropdown.open .dropdown-menu li:hover .dropdown-menu.level2');
        var el2 = $('.rimac-header-public .nav.navbar-nav li.dropdown.open .dropdown-menu li:hover p');


        addClass('opacity-50', $('.rimac-header-public .nav.navbar-nav li.dropdown.open .dropdown-menu li a'));

        addClass('no-display', $('.rimac-header-public .nav.navbar-nav li.dropdown.open .dropdown-menu .dropdown-menu.level2'));

        $('.rimac-header-public .nav.navbar-nav li.dropdown.open .dropdown-menu li:hover a').removeClass('opacity-50');

        var wdth = $('.rimac-header-public .nav.navbar-nav li.dropdown.open .dropdown-menu').width();
        $('.rimac-header-public .nav.navbar-nav li.dropdown.open .dropdown-menu .dropdown-menu.level2').css('width', (wdth + 2));

        $('.rimac-header-public .square').each(function () {
            $(this).remove();
        });

        $('.rimac-header-public) .nav.navbar-nav li.dropdown.open .dropdown-menu li:hover .dropdown-menu.level2').removeClass('no-display');
        addSpecialArrow(el2, el);
    }
}

function removeOp(e) {
    try {
        if ($(window).width() >= 1206) {
            if (($(e.target).parent()[0].localName).indexOf('li') > -1 || ($(e.target).parent().parent()[0].localName).indexOf('li') > -1 || ($(e.target)[0].localName).indexOf('li') > -1) {
                $('.nav.navbar-nav li.dropdown.open .dropdown-menu .dropdown-menu.level2 li.level2Li a span').each(function () {
                    $(this).removeClass('targetting');
                });
                if (($(e.target)[0].localName).indexOf('span') <= -1) {
                    if (($(e.target).children[0].localName).indexOf('span') > -1) {
                        $(e.target).children('span').addClass('targetting');
                    } else {
                        if (($(e.target)[0].localName).indexOf('li') > -1) {
                            $(e.target).children('a').children('span').addClass('targetting');
                        } else {
                            $(e.target).parent().children('a').children('span').addClass('targetting');
                        }
                    }
                } else {
                    $(e.target).addClass('targetting');
                }
            }
        }
    } catch (err) {
    }

    $('.nav.navbar-nav li.dropdown.open .dropdown-menu .dropdown-menu.level2 li.level2Li a span').each(function () {
        if ($(window).width() >= 1206) {
            if ($(this).hasClass('targetting')) {
                $(this).removeClass('opacity-50');
            } else {
                $(this).addClass('opacity-50');
            }
        }
    })
}

function addClass(cls, el) {
    $(el).each(function () {
        $(this).addClass(cls);
    });
}

function addSpecialArrow(el2, el) {
    var newEl = '<div class="square"></div>';
    $(el).append(newEl);

    setArrowPosition(el2, el);
}

function setArrowPosition(el2, el1) {
    var neg = ($(window).width() - $(el1).width()) / 2;
    var el = $('.square');
    var pos = $(el2).offset();
    var pos2 = $(el1).offset();
    $(el).css('left', (pos.left - neg));
    $(el).css('top', -24);
}


/*  show third level menu on mobile
    it gives the aparience of changing the page
    but only hides some parts of the nav menu and shows another 
*/
function changePage(item) {
    if ($('.rimac-header-private').length > 0) {
        var $navToShow = $('#' + item);
        $navToShow.css('left', $(window).width());
        $('.nav.navbar-nav').css('display', 'none');
        $navToShow.show(0).animate({ left: '0px' });
    }
    else{
        item = item.split(' ').join('');
        var elToSelect;
        if ($('.enterprise').length > 0) {
            elToSelect = $('.dropdown-menu');
        } else {
            elToSelect = $('.dropdown-menu.level2')
        }

        $(elToSelect).each(function () {
            if (item == $(this).parent().children('a').text().split(' ').join('')) {
                var el = $('.' + $(this).parent().children('a').text().split(' ').join(''));
                var el1 = '<h4 class="third-lev-mobile" onclick="gimme3rdLvlBack(\'' + item + '\')">' + $(this).parent().children('a').text() + '</h4>';
                $(el).append(el1);
                $($(this).children('li')).each(function () {
                    var title2 = $(this).children('a').children('h4').text();
                    var el2 = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 third-lev-mobile">' +
                        '<a href="#" class="third-lev-mobile">' + title2 + '</a>' +
                        '</div>';

                    $(el).append(el2);
                });
            }
        });
        var width = $(window).width();
        $('.' + item).css('left', width);
        //    $('.nav.navbar-nav').animate({left: '-'+width+'px'}, 400, function(){

        $('.nav.navbar-nav').css('display', 'none');
        $('.' + item).show(0);
        $('.' + item).animate({ left: '0px' });
        //    })
    }
}

function gimme3rdLvlBack(item) {
    var width = $(window).width(),
        selectorKey,
        isPrivateHeader = $('.rimac-header-private').length > 0;
    if (isPrivateHeader) {
        selectorKey = '#';
    }
    else {
        selectorKey = '.';
    }
    $(selectorKey + item).animate({ left: width + 'px' }, 400, function () {
        if(!isPrivateHeader) $(selectorKey + item).empty();
        $(selectorKey + item).hide();
        $('.nav.navbar-nav').show();
        $('.nav.navbar-nav').animate({ left: '0px' })
    });
}

function changeClassLocation() {
    if ($(window).width() <= 1205) {
        $('.click-change-page').each(function () {
            $(this).removeClass('click-change-page');
            $(this).parent().parent().addClass('click-change-page');
        });
        $('.user').addClass('click-change-page');
        $('.click-change-page').click(function () {
            var textOfTheLi = $(this).children('a').text();
            changePage(textOfTheLi);
        });
    }
}

function addEditingMode() {
    var currentUrl = window.location.href;

    if (currentUrl.indexOf('template') > -1) {
        $('.cmp-header').addClass('editing-mode');
    }
}

$('.navbar-toggle').click(function () {
    $('body').toggleClass('no-scroll', $(this).hasClass('collapsed'));

    //Validate browser in Safari
    let currentBrowserPublica= detectNavigator();
    if (currentBrowserPublica=="Safari"){
        let navbarToggleState= $("button.navbar-toggle").hasClass("open");

            if(navbarToggleState==true){
                $(".header ~ div").fadeIn('slow')
            }else{
                $(".header ~ div").fadeOut('slow')
            }
    }

});



//Detect Browser
function detectNavigator(){
     
    let agentBrowserDetect = window.navigator.userAgent;
    let currentNavigatorPublica = ["Chrome", "Firefox", "Safari", "Opera", "Trident", "MSIE", "Edge"];
    for(var i in currentNavigatorPublica){
        if(agentBrowserDetect.indexOf( currentNavigatorPublica[i]) != -1 ){
            return currentNavigatorPublica[i];
        }
    }
}


