$( window ).on("load", function() { 
    if (localStorage.getItem('rememberedUser')) {
        var user = JSON.parse(localStorage.getItem('rememberedUser'));
        var tipoDocumento = user.tipoDocumento;
        var numeroDocumento = user.numeroDocumento;
        var email = user.correo;
        var nombreCompleto = `${user.nombreUsuario} ${user.apePaternoUsuario} ${user.apeMaternoUsuario}`;
        var telefono = user.numeroTelefono;
        var aList = $(".card a.col")
        aList.toArray().forEach(element => {
          var href = element.getAttribute('href')
          element.setAttribute('href' ,`${href}&vi=${utf8_to_b64(numeroDocumento)}&ca=${utf8_to_b64(email)}&hu=${utf8_to_b64(nombreCompleto)}&co=${utf8_to_b64(telefono)}`)
        });
    }  
})

function utf8_to_b64( str ) {
    return window.btoa(unescape(encodeURIComponent( str )));
}
 
$(document).ready(function () {
  addCarousel("reason-carousel", 1, 4, 10, 40, 1, 0);
  function addCarousel(element, mobItems, deskItems, margin, marginDesktop, tabItems, margintablet) {
      $("#" + element).owlCarousel({
          loop: true,
          margin: 10,
          nav: false,
          responsive: {
              0: {
                  items: 1,
              },
              600: {
                  items: 2,
                  margin: 24
              },
              1050: {
                  items: 4,
                  margin: 40,
                  mouseDrag: false
              }
          }
      });
  }


  if (window.matchMedia("(min-width: 768px)").matches) {

        // Agregando Background
        var bgBanner = $('#bannerPromociones').attr("data-bgBanner");
        $('#banner').css('background-image', 'url(' + bgBanner + ')');

        var bg1 = $('#card_uno').data("imgdesk")
        $('#card_uno').css('background','url(' + bg1 +')');

        var bg2 = $('#card_dos').data('imgdesk')
        $('#card_dos').css('background','url(' + bg2 +')');

        var bg3 = $('#card_tres').data('imgdesk')
        $('#card_tres').css('background','url(' + bg3 +')');

        var bg4 = $('#card_cuatro').data('imgdesk')
        $('#card_cuatro').css('background','url(' + bg4 +')');

        var bg5 = $('#card_cinco').data('imgdesk')
        $('#card_cinco').css('background','url(' + bg5 +')');

        var bg6 = $('#card_seis').data('imgdesk')
        $('#card_seis').css('background','url(' + bg6 +')');
            } else {

        var bg1Mob = $('#card_uno').data('imgmob');
        $('#card_uno').css('background','url(' + bg1Mob +')');

        var bg2Mob = $('#card_dos').data('imgmob');
        $('#card_dos').css('background','url(' + bg2Mob +')');

        var bg3Mob = $('#card_tres').data('imgmob');
        $('#card_tres').css('background','url(' + bg3Mob +')');

        var bg4Mob = $('#card_cuatro').data('imgmob');
        $('#card_cuatro').css('background','url(' + bg4Mob +')');

        var bg5Mob = $('#card_cinco').data('imgmob');
        $('#card_cinco').css('background','url(' + bg5Mob +')');
        var bg6Mob = $('#card_seis').data('imgmob');
        $('#card_seis').css('background','url(' + bg6Mob +')');
        }
});